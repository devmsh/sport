import Croppa from 'vue-croppa';

Nova.booting((Vue, router) => {
    Vue.component('index-image-cropper', require('./components/IndexField'));
    Vue.component('detail-image-cropper', require('./components/DetailField'));
    Vue.component('form-image-cropper', require('./components/FormField'));
    Vue.use(Croppa);
})
