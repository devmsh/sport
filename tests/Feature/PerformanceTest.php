<?php

namespace Tests\Feature;

use App\League;
use App\Season;
use App\Team;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Tests\TestCase;

class PerformanceTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->withoutMiddleware(ThrottleRequests::class);
    }

    public function test_leagues_will_be_cached()
    {
        $this->assertKeyCached('leagues');
        $this->get('api/leagues');
    }

    public function test_teams_will_be_cached()
    {
        $this->assertKeyCached('teams');
        $this->get('api/teams');
    }

    public function test_league_will_be_cached()
    {
        factory(Season::class,2)->create()->each(function($season){
            $season->teams()->saveMany(factory(Team::class,10)->create());
        });

        $this->assertKeyCached('league-1');
        $this->get('api/leagues/1');
    }

    public function test_team_will_be_cached()
    {
        factory(Team::class,2)->create();

        $this->assertKeyCached('team-1');
        $this->get('api/teams/1');
    }
}
