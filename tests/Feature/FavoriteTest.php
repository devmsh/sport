<?php

namespace Tests\Feature;

use App\Favorite;
use App\League;
use App\Match;
use App\Team;
use App\User;
use Tests\TestCase;

class FavoriteTest extends TestCase
{
    public function test_user_can_favorite_a_league()
    {
        $this->loginAsNewUser();
        $league = factory(League::class)->create();
        $response = $this->post("api/favorites", [
            "type" => "league",
            "id" => $league->id,
        ]);

        $response->assertJson([
            'status' => true,
            'result' => ['action' => true]
        ]);
    }

    public function test_user_can_favorite_a_team()
    {
        $this->loginAsNewUser();
        $team = factory(Team::class)->create();

        $response = $this->post("api/favorites", [
            "type" => "team",
            "id" => $team->id,
        ]);

        $response->assertJson([
            'status' => true,
            'result' => ['action' => true]
        ]);
    }

    public function test_user_can_favorite_a_match()
    {
        $this->loginAsNewUser();
        $match = factory(Match::class)->create();

        $response = $this->post("api/favorites", [
            "type" => "match",
            "id" => $match->id,
        ]);

        $response->assertJson([
            'status' => true,
            'result' => ['action' => true]
        ]);
    }

    public function test_user_can_remove_item_from_favorite()
    {
        $user = $this->loginAsNewUser();
        $league = factory(League::class)->create();
        Favorite::toggle($user,'league',$league->id);

        $response = $this->post("api/favorites", [
            "type" => "league",
            "id" => $league->id,
        ]);

        $response->assertJson([
            'status' => true,
            'result' => ['action' => false]
        ]);
    }

    public function test_user_cant_favorite_unsupported_types()
    {
        $this->loginAsNewUser();
        $match = factory(Match::class)->create();

        $response = $this->post("api/favorites", [
            "type" => "x",
            "id" => $match->id,
        ]);

        $response->assertStatus(500);
    }

    public function test_user_can_get_his_favorites()
    {
        // create new user to associate it with the favorites
        $user = $this->loginAsNewUser();
        factory(Favorite::class, 2)->create([
            'user_id' => $user->id
        ]);

        $response = $this->get("api/favorites" );

        $response->assertJsonStructure([
            [
                'id',
                'favoritable_type',
                'favoritable',
            ]
        ]);
    }
}
