<?php

namespace Tests\Feature;

use App\Match;
use App\MatchTeam;
use App\MatchTeamPrediction;
use App\StagePrediction;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StagePredictionTest extends TestCase
{
    public function test_match_prediction_with_team_prediction()
    {
        $this->withoutExceptionHandling();

        $match = factory(Match::class)->create([
            'started_at' => Carbon::tomorrow(),
        ]);

        $match1 = factory(Match::class)->create([
            'started_at' => Carbon::tomorrow(),
            'winner_to_id' => $match->id,
            'winner_to_position' => Match::POSITION_FIRST
        ]);

        $match2 = factory(Match::class)->create([
            'started_at' => Carbon::tomorrow(),
            'winner_to_id' => $match->id,
            'winner_to_position' => Match::POSITION_SECOND
        ]);

        $this->loginAsNewUser();
        $response = $this->postJson("api/matches/{$match1->id}/resultPredictions", [
            'first_team_result' => 2,
            'second_team_result' => 1,
        ]);

        $response = $this->postJson("api/matches/{$match2->id}/resultPredictions", [
            'first_team_result' => 2,
            'second_team_result' => 1,
        ]);

        $response = $this->postJson("api/matches/{$match2->id}/resultPredictions", [
            'first_team_result' => 1,
            'second_team_result' => 2,
        ]);

        $this->assertEquals(1, StagePrediction::count());
        $this->assertEquals(1, MatchTeamPrediction::count());
    }

    public function test_can_predict_stage_16_matches_before_stage_start()
    {
        $this->withoutExceptionHandling();

        $this->loginAsNewUser();
        $stage = $this->createStage16Matches();

        $response = $this->postJson("api/stages/{$stage->id}/predictions", $this->data());

        $response->assertSuccessful();
        $this->assertEquals(1, StagePrediction::count());
        $this->assertEquals(5, MatchTeamPrediction::first()->first_team_id);
        $this->assertEquals(8, MatchTeamPrediction::first()->second_team_id);
    }

    public function test_can_predict_stage_16_matches_during_stage()
    {
        $this->loginAsNewUser();
        $stage = $this->createStage16Matches();

        // finish 5 matches
        Match::take(5)->update(['started_at' => Carbon::yesterday()]);

        $response = $this->postJson("api/stages/{$stage->id}/predictions", $this->data());


        $response->assertSuccessful();
        $this->assertEquals(5, MatchTeamPrediction::first()->first_team_id);
        $this->assertEquals(8, MatchTeamPrediction::first()->second_team_id);
        $this->assertEquals(5, StagePrediction::first()->finished_matches);
    }

    public function test_cant_predict_stage_16_matches_after_last_match_start()
    {
        $this->markTestSkipped("requirement need to be change");
        $this->loginAsNewUser();
        $stage = $this->createStage16Matches();

        // finish all matches
        Match::take(8)->update(['started_at' => Carbon::yesterday()]);

        $response = $this->postJson("api/stages/{$stage->id}/predictions", $this->data());

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['stage']);
    }

    public function test_user_can_get_list_of_stages_predictions()
    {
        $this->loginAsNewUser();
        $stage = $this->createStage16Matches();

        StagePrediction::predict([$stage, [
            "A1" => 1, "A2" => 2, "B1" => 3, "B2" => 4, "C1" => 5, "C2" => 6, "D1" => 7, "D2" => 8,
            "E1" => 9, "E2" => 10, "F1" => 11, "F2" => 12, "G1" => 13, "G2" => 14, "H1" => 15, "H2" => 16,
        ], ""]);

        $stage = $this->createStage8Matches();

        StagePrediction::predict([$stage, [
            "W49" => 1, "W56" => 2, "W51" => 3, "W54" => 4,
            "W50" => 5, "W55" => 6, "W52" => 7, "W53" => 8
        ], ""]);

        $response = $this->getJson("api/predictions/{$stage->season_id}/stages");

        $response->assertSuccessful();
        $response->assertJsonStructure([[
            'stage',
            'finished_matches',
            'status',
            'weight'
        ]]);
    }

    /**
     * @return array
     */
    public function data(): array
    {
        return [
            "groups" => [
                [
                    "name" => "A",
                    "teams" => [
                        ["id" => 1],
                        ["id" => 2],
                    ]
                ],
                [
                    "name" => "B",
                    "teams" => [
                        ["id" => 3],
                        ["id" => 4],
                    ]
                ],
                [
                    "name" => "C",
                    "teams" => [
                        ["id" => 5],
                        ["id" => 6],
                    ]
                ],
                [
                    "name" => "D",
                    "teams" => [
                        ["id" => 7],
                        ["id" => 8],
                    ]
                ],
                [
                    "name" => "E",
                    "teams" => [
                        ["id" => 9],
                        ["id" => 10],
                    ]
                ],
                [
                    "name" => "F",
                    "teams" => [
                        ["id" => 11],
                        ["id" => 12],
                    ]
                ],
                [
                    "name" => "G",
                    "teams" => [
                        ["id" => 13],
                        ["id" => 14],
                    ]
                ],
                [
                    "name" => "H",
                    "teams" => [
                        ["id" => 15],
                        ["id" => 16],
                    ]
                ],
            ]
        ];
    }
}
