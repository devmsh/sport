<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class AuthTest extends TestCase
{
    var $userFields = [
        'id',
        'name',
        'photo',
        'rank',
        'points',
        'predictions_count',
        'favorites_count',
    ];

    public function setUp()
    {
        parent::setUp();

        $this->mockFacebookSDK();
    }

    public function test_user_can_login_by_fb_info()
    {
        $this->mockFacebookSDK();
        $this->mockFacebookFriends();

        \Artisan::call('passport:install');
        $token = "fakeToken";
        $response = $this->post('api/users/login', ['access_token' => $token]);

        $response->assertJsonStructure($this->userFields);
        // 25 friends from the mockedFacebookFriends
        $this->assertCount(25 + 1, User::all());
    }

    public function test_user_did_not_duplicated_for_the_same_fb_user()
    {
        $this->post('api/users/login', ['access_token' => "fakeToken"]);
        $this->post('api/users/login', ['access_token' => "fakeToken"]);

        $this->assertCount(1, User::all());
    }

    public function test_can_get_my_profile()
    {
        $this->loginAsNewUser();
        $response = $this->get('api/me');

        $response->assertJsonStructure($this->userFields);
    }

    public function test_user_complete_guide()
    {
        $this->loginAsNewUser();
        $response = $this->get('api/me/complete');

        $response->assertJsonStructure($this->userFields);
    }
}
