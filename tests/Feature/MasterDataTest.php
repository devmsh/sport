<?php

namespace Tests\Feature;

use App\League;
use App\Season;
use App\Team;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class MasterDataTest extends TestCase
{
    public function test_guests_can_see_the_app_story()
    {
        $response = $this->get('api/guide');
        $response->assertJsonStructure([
            [
                'image',
                'title',
                'description'
            ]
        ]);
    }

    public function test_user_can_get_leagues()
    {
        factory(League::class, 15)->create();

        $response = $this->get('api/leagues');

        $response->assertJsonStructure([
            [
                'id',
                'name',
                'logo',
                'leagues' => [
                    [
                        'id',
                        'name',
                        'logo',
                        'followers_count',
//                        'country_name',
                    ]
                ]
            ]
        ]);
    }

    public function test_user_can_get_league()
    {
        factory(League::class, 2)->create();

        $response = $this->get('api/leagues/1');

        $response->assertJsonStructure([
            'id',
            'name',
            'logo',
            'followers_count',
//            'country_name',
            'followers_count'
        ]);
    }

    public function test_user_cant_get_invalid_league()
    {
        $response = $this->get('api/leagues/1');

        $response->assertStatus(404);
    }

    public function test_user_can_get_all_teams()
    {
        factory(Team::class, 15)->create();

        $response = $this->get('api/teams');

        $response->assertJsonStructure([
            [
                'id',
                'name',
                'logo',
            ]
        ]);
    }

    public function test_user_can_get_teams_by_league()
    {
        $teams = factory(Team::class, 15)->create();
        factory(League::class)->create();
        $season = factory(Season::class)->create(["league_id" => 1]);
        $season->teams()->attach($teams);

        $response = $this->get('api/teams?league_id=1');

        $response->assertSuccessful();
        $response->assertJsonStructure([
            [
                'id',
                'name',
                'logo',
            ]
        ]);
    }

    public function test_user_cant_get_teams_by_invalid_league()
    {
        $response = $this->get('api/teams?league_id=1', ['Accept' => 'application/json']);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors([
            "league_id"
        ]);
    }

    public function test_user_can_get_team()
    {
        factory(Team::class, 15)->create();

        $response = $this->get('api/teams/1');

        $response->assertJsonStructure([
            'id',
            'name',
            'logo',
            'followers_count',
        ]);
    }

    public function test_user_cant_get_invalid_team()
    {
        $response = $this->get('api/teams/1');

        $response->assertStatus(404);
    }
}
