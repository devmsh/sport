<?php

namespace Tests\Feature;

use App\Suggestion;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class SuggestionsTest extends TestCase
{
    use DatabaseMigrations;

    public function test_can_predict_leagues()
    {
        $this->withoutExceptionHandling();
        $this->loginAsNewUser();
        $result = $this->postJson('api/suggestions/league',[
            'name' => 'La liga'
        ]);

        $result = $this->postJson('api/suggestions/league',[
            'name' => 'La liga'
        ]);

        $result->assertSuccessful();
        $this->assertEquals(1,Suggestion::count());

        $result = $this->postJson('api/suggestions/league',[
            'name' => 'La liga1'
        ]);

        $result->assertSuccessful();
        $result->assertJsonStructure([
            'status',
            'message'
        ]);
        $this->assertEquals(2,Suggestion::count());
    }
}
