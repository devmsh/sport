<?php

namespace Tests\Feature;

use App\Friend;
use App\User;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Zend\Http\Header\From;

class SocialTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->mockFacebookSDK();
    }

    public function test_get_user_friends_after_login()
    {
        $this->mockFacebookFriends();

        \Artisan::call('passport:install');

        $response = $this->post('api/users/login', ['access_token' => "fakeToken"]);

        $friends = Friend::all();

        $response->assertSuccessful();
        $this->assertCount(25, $friends);
        $this->assertEquals("10101842399947753", $friends->first()->facebook_id);
    }

    public function test_user_can_get_match_details_with_social_info()
    {
        $match = $this->createMatch();
        $results = [[1, 2], [2, 1], [2, 2], [1, 1], [2, 3], [1, 3]];

        foreach ($results as $result) {
            $this->loginAsNewUser();
            [$firstTeam, $secondTeam] = $result;
            $match->predict($firstTeam, $secondTeam);
        }

        $response = $this->get('api/matches/1');

        $response->assertJson([
            'stats' => [
                'first_team_count' => 1,
                'second_team_count' => 3,
                'draw_count' => 2,
                'first_team_percentage' => 1 / 6 * 100,
                'second_team_percentage' => 3 / 6 * 100,
                'draw_percentage' => 2 / 6 * 100,
            ]
        ]);
    }

    public function test_user_can_get_match_details_with_friends_lists()
    {
        $match = $this->createMatch();

        $results = [[1, 2], [2, 1], [2, 2], [1, 1], [2, 3], [1, 3]];

        foreach ($results as $result) {
            $this->loginAsNewUser();
            [$firstTeam, $secondTeam] = $result;
            $match->predict($firstTeam, $secondTeam);
        }

        $this->makeAllUsersFriends();

        Passport::actingAs(User::find(1));
        $response = $this->get('api/matches/1');

        $response->assertJsonStructure([
            'stats' => [
                'first_team_friends' => [['id', 'name', 'photo']],
                'second_team_friends' => [['id', 'name', 'photo']],
                'draw_friends' => [['id', 'name', 'photo']],
            ]
        ]);

        $this->assertCount(1, $response->json('stats.first_team_friends'));
        // The user only see friends list not himself so the second team count is 2 not 3
        $this->assertCount(2, $response->json('stats.second_team_friends'));
        $this->assertCount(2, $response->json('stats.draw_friends'));
    }

    public function test_user_can_only_see_his_friends_in_match_details()
    {
        $match = $this->createMatch();

        $results = [[1, 2], [2, 1], [2, 2], [1, 1], [2, 3], [1, 3]];

        foreach ($results as $result) {
            $this->loginAsNewUser();
            [$firstTeam, $secondTeam] = $result;
            $match->predict($firstTeam, $secondTeam);
        }

        $this->makeAllUsersFriends();

        // THESE DUPLICATED PREDICTION TO CREATE NEW NON FRIENDS USERS
        foreach ($results as $result) {
            $this->loginAsNewUser();
            [$firstTeam, $secondTeam] = $result;
            $match->predict($firstTeam, $secondTeam);
        }

        Passport::actingAs(User::find(1));
        $response = $this->get('api/matches/1');

        $response->assertSuccessful();
        $response->assertJsonStructure([
            'stats' => [
                'first_team_friends' => [['id', 'name', 'photo']],
                'second_team_friends' => [['id', 'name', 'photo']],
                'draw_friends' => [['id', 'name', 'photo']],
            ]
        ]);

        $this->assertCount(1, $response->json('stats.first_team_friends'));
        $this->assertCount(2, $response->json('stats.second_team_friends'));
        $this->assertCount(2, $response->json('stats.draw_friends'));
    }

    private function makeAllUsersFriends()
    {
        $users = User::all();
        foreach ($users as $user) {
            foreach ($users as $friend) {
                if ($user->id != $friend->id) {
                    $user->friends()->create([
                        'facebook_id' => $user->fb_id,
                        'friend_id' => $friend->id
                    ]);
                }
            }
        }
    }

    public function test_user_complete_guide()
    {
        $user1 = $this->loginAsNewUser();
        $user2 = $this->loginAsNewUser();
        $response = $this->postJson("api/friends/{$user1->id}");
        $response->assertSuccessful();
        $this->assertTrue($response->json('result.action'));

        $response = $this->postJson("api/friends/{$user1->id}");
        $response->assertSuccessful();
        $this->assertFalse($response->json('result.action'));
    }

}
