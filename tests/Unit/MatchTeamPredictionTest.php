<?php

namespace Tests\Unit;

use App\Events\MatchIdsSet;
use App\Events\StageMatchIdsSet;
use App\Match;
use App\MatchResultPrediction;
use App\MatchTeam;
use App\MatchTeamPrediction;
use App\Stage;
use App\StagePrediction;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class MatchTeamPredictionTest extends TestCase
{
    public function test_can_evaluate_match_team_when_set_team_ids()
    {
        Event::fake();
        $this->loginAsNewUser();
        $stage = $this->createStage16Matches();
        StagePrediction::predict([$stage, [
            "A1" => 1, "A2" => 2, "B1" => 3, "B2" => 4, "C1" => 5, "C2" => 6, "D1" => 7, "D2" => 8,
            "E1" => 9, "E2" => 10, "F1" => 11, "F2" => 12, "G1" => 13, "G2" => 14, "H1" => 15, "H2" => 16,
        ]]);

        $match = Match::find(1);
        $match->setTeamsIds(1, 2);

        Event::assertDispatched(MatchIdsSet::class);
    }

    public function test_evaluate_match_team_status_as_exact_when_prediction_teams_both_right()
    {
        $this->loginAsNewUser();
        $stage = $this->createStage16Matches();
        StagePrediction::predict([$stage, [
            "A1" => 1, "A2" => 2, "B1" => 3, "B2" => 4, "C1" => 5, "C2" => 6, "D1" => 7, "D2" => 8,
            "E1" => 9, "E2" => 10, "F1" => 11, "F2" => 12, "G1" => 13, "G2" => 14, "H1" => 15, "H2" => 16,
        ]]);

        $match = Match::find(1);
        $match->setTeamsIds(5, 8);

        $this->assertEquals(MatchTeamPrediction::STATUS_EXACT, MatchTeamPrediction::find(1)->status);
    }

    public function test_evaluate_match_team_status_as_only_first_team_right_when_prediction_teams()
    {
        $this->loginAsNewUser();
        $stage = $this->createStage16Matches();
        StagePrediction::predict([$stage, [
            "A1" => 1, "A2" => 2, "B1" => 3, "B2" => 4, "C1" => 5, "C2" => 6, "D1" => 7, "D2" => 8,
            "E1" => 9, "E2" => 10, "F1" => 11, "F2" => 12, "G1" => 13, "G2" => 14, "H1" => 15, "H2" => 16,
        ]]);

        $match = Match::find(1);
        $match->setTeamsIds(5, 99);

        $this->assertEquals(MatchTeamPrediction::STATUS_FIRST_TEAM_RIGHT, MatchTeamPrediction::find(1)->status);
    }

    public function test_evaluate_match_team_status_as_only_second_team_right_when_prediction_teams()
    {
        $this->loginAsNewUser();
        $stage = $this->createStage16Matches();
        StagePrediction::predict([$stage, [
            "A1" => 1, "A2" => 2, "B1" => 3, "B2" => 4, "C1" => 5, "C2" => 6, "D1" => 7, "D2" => 8,
            "E1" => 9, "E2" => 10, "F1" => 11, "F2" => 12, "G1" => 13, "G2" => 14, "H1" => 15, "H2" => 16,
        ]]);

        $match = Match::find(1);
        $match->setTeamsIds(99, 8);

        $this->assertEquals(MatchTeamPrediction::STATUS_SECOND_TEAM_RIGHT, MatchTeamPrediction::find(1)->status);
    }

    public function test_evaluate_match_team_status_as_failed_when_prediction_teams()
    {
        $this->loginAsNewUser();
        $stage = $this->createStage16Matches();
        StagePrediction::predict([$stage, [
            "A1" => 1, "A2" => 2, "B1" => 3, "B2" => 4, "C1" => 5, "C2" => 6, "D1" => 7, "D2" => 8,
            "E1" => 9, "E2" => 10, "F1" => 11, "F2" => 12, "G1" => 13, "G2" => 14, "H1" => 15, "H2" => 16,
        ]]);

        $match = Match::find(1);
        $match->setTeamsIds(2, 3);

        $this->assertEquals(MatchTeamPrediction::STATUS_LOSE, MatchTeamPrediction::find(1)->status);
    }

    public function test_can_evaluate_stage()
    {
        $this->markTestSkipped('stage prediction need to be REDEVELOP');
        $this->loginAsNewUser();
        $stage = $this->createStage16Matches();
        StagePrediction::predict([$stage, [
            "A1" => 1, "A2" => 2, "B1" => 3, "B2" => 4, "C1" => 5, "C2" => 6, "D1" => 7, "D2" => 8,
            "E1" => 9, "E2" => 10, "F1" => 11, "F2" => 12, "G1" => 13, "G2" => 14, "H1" => 15, "H2" => 16,
        ]]);

        Match::find(1)->match->allowToFinish()->evaluate();
        Match::find(2)->match->allowToFinish()->evaluate();
        Match::find(3)->match->allowToFinish()->evaluate();
        Match::find(4)->match->allowToFinish()->evaluate();
        Match::find(5)->match->allowToFinish()->evaluate();
        Match::find(6)->match->allowToFinish()->evaluate();
        Match::find(7)->match->allowToFinish()->evaluate();
        Match::find(8)->match->allowToFinish()->evaluate();

        $stage = $stage->fresh();
        $this->assertEquals(1, $stage->evaluated);
    }

    public function test_can_get_the_winner_from_mrp()
    {
        $match = $this->createMatch();

        // no winner yet
        $this->assertNull($match->winner());

        $this->loginAsNewUser();
        $mrp = $match->predict(1, 2);

        // can get predictedWinner from mrp
        $this->assertEquals($match->teams->secondTeam->id, $mrp->winner()->id);

        // but it's not yet a real winner
        $this->assertNull($match->winner());

        $match->started_at = Match::markAsFinished();
        $match->finish(2, 1);

        // once the match finished, the real winner appear
        $this->assertEquals($match->teams->firstTeam->id, $match->winner()->id);
    }

    public function test_future_prediction_updated()
    {
        $firstMatch = $this->createMatch();
        $secondMatch = $this->createMatch();
        $futureMatch = $this->createLabeledMatch();

        $firstMatch->winnerTo()->associate($futureMatch);
        $firstMatch->winner_to_position = Match::POSITION_FIRST;
        $firstMatch->save();

        $secondMatch->winnerTo()->associate($futureMatch);
        $secondMatch->winner_to_position = Match::POSITION_SECOND;
        $secondMatch->save();

        $this->loginAsNewUser();
        $firstMatch->predict(1,2);
        $this->assertEquals($firstMatch->teams->secondTeam,$futureMatch->myTeamPrediction->firstTeam);

        $firstMatch->predict(2,1);
        $this->assertEquals($firstMatch->fresh()->teams->firstTeam,$futureMatch->fresh()->myTeamPrediction->firstTeam);


        $firstMatch->allowToFinish()->finish(1,2);
        $secondMatch->allowToFinish()->finish(2,1);

        $this->assertEquals($firstMatch->teams->secondTeam,$futureMatch->teams->firstTeam);
        $this->assertEquals($secondMatch->teams->firstTeam,$futureMatch->teams->secondTeam);

    }
}