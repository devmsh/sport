<?php

namespace Tests\Unit;

use App\Jobs\MatchEvaluationNotificationJob;
use App\Match;
use App\MatchResultPrediction;
use App\MatchTeam;
use Mockery;
use Tests\TestCase;

class MatchEvaluationNotificationJobTest extends TestCase
{
    public function test_notification_sent_to_the_four_type_of_predictions()
    {
        /** @var Match $match */
        $match = factory(Match::class)->create();

        factory(MatchResultPrediction::class)->states('lose')->create([
            'match_id' => $match->id
        ]);

        factory(MatchResultPrediction::class)->states('win')->create([
            'match_id' => $match->id
        ]);

        factory(MatchResultPrediction::class)->states('exact')->create([
            'match_id' => $match->id
        ]);

        factory(MatchResultPrediction::class)->states('diff')->create([
            'match_id' => $match->id
        ]);

        $sender = Mockery::mock(\LaravelFCM\Sender\FCMSender::class);
        $sender->shouldReceive('sendTo')->withArgs(function (){
            return true;
        })->times(4);

        $this->app->singleton('fcm.sender', function($app) use($sender) {
            return $sender;
        });

        \Queue::push(new MatchEvaluationNotificationJob($match));
    }
}
