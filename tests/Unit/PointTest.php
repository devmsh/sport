<?php

namespace Tests\Unit;

use App\Leaderboard;
use App\Prediction;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PointTest extends TestCase
{
    public function test_user_has_point()
    {
        $user = factory(User::class)->create([
            'points' => 100
        ]);

        $this->assertEquals(100,$user->points);
    }

    public function test_user_has_points_history()
    {
        $user = factory(User::class)->create();
        $user->addPoints(1000,"First registration");

        $this->assertCount(1, $user->pointsHistory);
        $pointHistory = $user->pointsHistory()->first();
        $this->assertEquals(1000, $pointHistory->points);
        $this->assertEquals(1000, $user->points);

        $user->addPoints(500,"Gift");

        $this->assertCount(2, $user->fresh()->pointsHistory);
        $this->assertEquals(1500, $user->points);
    }

    public function test_points_can_be_linked_to_reason()
    {
        $prediction = factory(Prediction::class)->create();
        $user = factory(User::class)->create();
        $user->addPoints(50,"Win Match Prediction",$prediction);

        $pointsHistory = $user->pointsHistory()->first();
        $this->assertEquals(50, $user->points);
        $this->assertEquals("Win Match Prediction", $pointsHistory->message);
        $this->assertEquals($prediction->id, $pointsHistory->reason()->first()->id);
    }

    public function test_leaderboard_view_keys()
    {
        $this->markTestSkipped('leaderboard chaged');
        $prediction = factory(Prediction::class)->create();
        $firstUser = factory(User::class)->create();
        $firstUser->addPoints(100,"",$prediction );

        $prediction = factory(Prediction::class)->create();
        $secondUser = factory(User::class)->create();
        $secondUser->addPoints(50,"",$prediction );

        $match = $prediction->predictable->match;

        $seasonLeaderboard = Leaderboard::top(10,$match->season)->first();
        $this->assertEquals($secondUser->id,$seasonLeaderboard->id);

        $seasonLeaderboard = Leaderboard::top(10,$match->season->league)->first();
        $this->assertEquals($secondUser->id,$seasonLeaderboard->id);

        $leaderboard = Leaderboard::top()->first();
        $this->assertEquals($firstUser->id,$leaderboard->id);
    }
}
