<?php

namespace Tests\Unit;

use App\Match;
use App\MatchTeam;
use App\Season;
use App\Stage;
use App\StagePrediction;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StageTest extends TestCase
{
    public function test_stage_status_scheduled_if_all_match_scheduled()
    {
        $stage = factory(Stage::class)->create();

        $this->createMatchAt($stage, Carbon::tomorrow());
        $this->createMatchAt($stage, Carbon::tomorrow());

        $this->assertEquals(Stage::STATUS_SCHEDULED, $stage->status);
    }

    public function test_stage_status_live_if_not_all_match_finished()
    {
        $stage = factory(Stage::class)->create();

        $this->createMatchAt($stage, Carbon::tomorrow());
        $this->createMatchAt($stage, Carbon::yesterday());

        $this->assertEquals(Stage::STATUS_LIVE, $stage->status);
    }

    public function test_stage_status_finished_if_all_match_finished_and_evaluated()
    {
        $stage = factory(Stage::class)->create();

        $this->createMatchAt($stage, Carbon::yesterday())->update(['status' => Match::STATUS_EVALUATED]);
        $this->createMatchAt($stage, Carbon::yesterday())->update(['status' => Match::STATUS_EVALUATED]);;

        $this->assertEquals(Stage::STATUS_FINISHED, $stage->status);
    }

    public function test_stage_status_finish_if_all_match_evaluated_but_stage_not_evaluated()
    {
        $stage = factory(Stage::class)->create();

        $this->createMatchAt($stage, Carbon::yesterday())->update(['status' => Match::STATUS_EVALUATED]);
        $this->createMatchAt($stage, Carbon::yesterday())->update(['status' => Match::STATUS_EVALUATED]);;

        $this->assertEquals(Stage::STATUS_FINISHED, $stage->status);
    }

    public function test_can_get_the_count_of_stage_matches_per_status()
    {
        /** @var Stage $stage */
        $stage = factory(Stage::class)->create();

        $this->createMatchAt($stage, Carbon::tomorrow());
        $this->createMatchAt($stage, Carbon::tomorrow());
        $this->createMatchAt($stage, Carbon::yesterday());
        $this->createMatchAt($stage, Carbon::yesterday());
        $this->createMatchAt($stage, Carbon::yesterday())->update(['status' => Match::STATUS_EVALUATED]);

        $this->assertEquals(5, $stage->matches()->count());
        $this->assertEquals(2, $stage->scheduledMatches()->count());
        $this->assertEquals(3, $stage->finishMatches()->count());
        $this->assertEquals(1, $stage->evaluatedMatches()->count());
    }

    public function test_stage_must_have_weight()
    {
        $stage = factory(Stage::class)->create([
            'weight' => 300
        ]);

        $this->assertEquals(300, $stage->fresh()->weight);
    }

    public function test_can_evaluate_a_stage_if_all_matches_evaluated()
    {
        /** @var Stage $stage */
        $stage = factory(Stage::class)->create([
            'weight' => 500
        ]);
        $stagePrediction = factory(StagePrediction::class)->create([
            'stage_id' => $stage->id,
        ]);

        $this->createMatchAt($stage, Carbon::yesterday())->update(['status' => Match::STATUS_EVALUATED]);
        $this->createMatchAt($stage, Carbon::yesterday())->update(['status' => Match::STATUS_EVALUATED]);

        $this->assertEquals(StagePrediction::STATUS_PENDING, $stagePrediction->status); // todo must be deleted after the real implementation

        $stage->evaluate();

        $stagePrediction = $stagePrediction->fresh();

        $this->assertEquals(Stage::STATUS_EVALUATED, $stage->status);
        $this->assertEquals(500, $stagePrediction->weight); // todo must deleted after the real implementation
        $this->assertEquals(StagePrediction::STATUS_PENDING, $stagePrediction->status); // todo must deleted after the real implementation
    }

    public function test_cannot_evaluate_a_stage_if_not_all_matches_evaluated()
    {
        /** @var Stage $stage */
        $stage = factory(Stage::class)->create();

        $this->createMatchAt($stage, Carbon::yesterday());
        $this->createMatchAt($stage, Carbon::yesterday())->update(['status' => Match::STATUS_EVALUATED]);

        $this->expectException(\Exception::class);

        $stage->evaluate();

        $this->assertEquals(Stage::STATUS_FINISHED, $stage->status);
    }

    private function createMatchAt($stage, $started_at)
    {
        $match = factory(Match::class)->create([
            'season_id' => $stage->season->id,
            'stage_id' => $stage->id,
            'started_at' => $started_at
        ]);

        return $match;
    }
}
