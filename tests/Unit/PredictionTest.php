<?php

namespace Tests\Unit;

use App\AbstractPrediction;
use App\Jobs\MatchEvaluationNotificationJob;
use App\Match;
use App\MatchResultPrediction;
use App\MatchTeam;
use App\MatchTeamPrediction;
use App\Point;
use App\Prediction;
use App\StagePrediction;
use App\User;
use App\UserMatchResult;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Queue;
use LaravelFCM\Facades\FCM;
use Mockery\Matcher\Any;
use Mockery\Matcher\AnyArgs;
use Mockery\Mock;
use Symfony\Component\Process\Process;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PredictionTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        Notification::fake();
    }

    public function test_user_can_predict_match_result()
    {
        $match = $this->createMatch();

        $this->loginAsNewUser();
        $prediction = $match->predict(1, 2);

        $this->assertEquals(0, $prediction->points);
        $this->assertEquals(1, $prediction->first_team_result);
        $this->assertEquals(2, $prediction->second_team_result);
    }

    public function test_predict_points_must_be_sub_from_user()
    {
        $match = $this->createMatch();

        $user = $this->loginAsNewUser();
        $this->assertEquals(1000, $user->fresh()->points);

        $prediction = $match->predict(1, 2);

        $this->assertEquals(1000 - MatchResultPrediction::USER_POINT_PER_PREDICTION, $user->fresh()->points);

        $this->assertEquals(0, $prediction->points);
        $this->assertEquals(1, $prediction->first_team_result);
        $this->assertEquals(2, $prediction->second_team_result);
    }

    public function test_prediction_defaults_to_pending()
    {
        $match = $this->createMatch();

        $this->loginAsNewUser();
        $prediction = $match->predict(1, 2);

        $this->assertEquals(MatchResultPrediction::STATUS_PENDING, $prediction->status);
    }

    public function test_match_can_be_finished()
    {
        $match = $this->createMatch(Carbon::yesterday());

        $match->finish(1, 2);

        $this->assertEquals(Match::STATUS_FINISHED, $match->status);
    }

    public function test_match_cannot_be_finished_before_the_time_end()
    {
        $match = $this->createMatch();

        $this->expectException(\Exception::class);
        $match->finish(1, 2);
    }

    public function test_cannot_predict_after_match_start()
    {
        $match = $this->createMatch(Carbon::now()->subMinutes(50));

        $this->expectException(\Exception::class);

        $this->loginAsNewUser();
        $match->predict(1, 2);
    }

    public function test_match_can_be_evaluated()
    {
        Queue::fake();

        $match = $this->createMatch();

        $this->loginAsNewUser();
        $exactPrediction = $match->predict(1, 2);

        $this->loginAsNewUser();
        $lostPrediction = $match->predict(2, 1);

        $this->loginAsNewUser();
        $winPrediction = $match->predict(1, 3);

        $this->loginAsNewUser();
        $diffPrediction = $match->predict(2, 3);

        // modify the match as it's started from 106 minute to simulate the match finish status
        $match->started_at = Match::markAsFinished();
        $match->finish(1, 2);
        $match->evaluate();

        $this->assertEquals(MatchResultPrediction::EXACT_WEIGHT, $exactPrediction->fresh()->weight);
        $this->assertEquals(MatchResultPrediction::LOSE_WEIGHT, $lostPrediction->fresh()->weight);
        $this->assertEquals(MatchResultPrediction::WINNER_WEIGHT, $winPrediction->fresh()->weight);
        $this->assertEquals(MatchResultPrediction::DIFF_WEIGHT, $diffPrediction->fresh()->weight);

        $this->assertEquals(MatchResultPrediction::STATUS_EXACT, $exactPrediction->fresh()->status);
        $this->assertEquals(MatchResultPrediction::STATUS_LOSE, $lostPrediction->fresh()->status);
        $this->assertEquals(MatchResultPrediction::STATUS_WIN, $winPrediction->fresh()->status);
        $this->assertEquals(MatchResultPrediction::STATUS_DIFF, $diffPrediction->fresh()->status);
    }

    public function test_match_evaluation_notification()
    {
        $match = $this->createMatch();

        $this->loginAsNewUser(true, ['device_token' => '1']);
        $match->predict(1, 2);

        $this->loginAsNewUser(true, ['device_token' => '2']);
        $match->predict(2, 1);

        $this->loginAsNewUser(true, ['device_token' => '3']);
        $match->predict(1, 3);

        $this->loginAsNewUser(true, ['device_token' => '4']);
        $match->predict(2, 3);

        $match->started_at = Match::markAsFinished();
        $match->finish(1, 2);

        Queue::fake();

        $match->evaluate();

        Queue::assertPushed(MatchEvaluationNotificationJob::class, function ($job) use ($match) {
            return $job->match == $match;
        });
    }

    public function test_user_must_have_points_to_predict()
    {
        $this->expectException(\Exception::class);

        $match = $this->createMatch();

        $this->loginAsNewUser(false);
        $match->predict(1, 2);
    }

    public function test_guest_can_not_predict_match()
    {
        $this->expectException(\Exception::class);

        $match = $this->createMatch();
        $match->predict(1, 3);
    }

    public function test_user_can_add_match_result()
    {
        $this->loginAsNewUser();

        $match = $this->createMatch(Carbon::yesterday());
        $match->addResult(1, 2);

        $this->assertCount(1, UserMatchResult::all());
    }

    public function test_user_can_update_match_result()
    {
        $this->loginAsNewUser();
        $this->loginAsNewUser();

        $match = $this->createMatch(Carbon::yesterday());
        $match->addResult(1, 2);
        $match->addResult(2, 3);

        $this->assertCount(1, UserMatchResult::all());
        $result = UserMatchResult::first();
        $this->assertEquals(2, $result->first_team_result);
        $this->assertEquals(3, $result->second_team_result);
    }

    public function test_guest_can_not_add_match_result()
    {
        $this->expectException(\Exception::class);

        $match = $this->createMatch(Carbon::yesterday());
        $match->addResult(1, 2);
    }

    public function test_can_not_add_match_result_before_match_end()
    {
        $match = $this->createMatch(Carbon::now()->subMinutes(50));

        $this->expectException(\Exception::class);

        $this->loginAsNewUser();
        $match->addResult(1, 2);
    }

    public function test_jackpot_algorithms_without_team_predictions()
    {
        $match = $this->createMatch();

        $this->loginAsNewUser();
        $exactPrediction = $match->predict(1, 2);

        $this->loginAsNewUser();
        $losePrediction = $match->predict(2, 1);

        $this->loginAsNewUser();
        $diffPrediction = $match->predict(2, 3);

        $this->loginAsNewUser();
        $winnerPrediction = $match->predict(1, 3);

        $match->started_at = Match::markAsFinished();
        $match->finish(1, 2);
        $match->evaluate();

        $this->assertEquals(21.333333333334, $exactPrediction->fresh()->points);
        $this->assertEquals(16.0000000000005, $diffPrediction->fresh()->points);
        $this->assertEquals(10.666666666667, $winnerPrediction->fresh()->points);
        $this->assertEquals(-10, $losePrediction->fresh()->points);
    }

    public function test_jackpot_algorithms_with_both_right_team_predictions()
    {
        $stage = $this->createStage16Matches();

        /** @var Match $match */
        $match = Match::find(1);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $exactPrediction = $match->predict(1, 2);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $losePrediction = $match->predict(2, 1);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $diffPrediction = $match->predict(2, 3);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $winnerPrediction = $match->predict(1, 3);

        Match::find(1)->setTeamsIds(5, 8);
        $match->started_at = Match::markAsFinished();
        $match->finish(1, 2);
        $match->evaluate();

        $this->assertEquals(42.666666666666, $exactPrediction->fresh()->points);
        $this->assertEquals(31.9999999999995, $diffPrediction->fresh()->points);
        $this->assertEquals(21.333333333333, $winnerPrediction->fresh()->points);
        $this->assertEquals(-10.0, $losePrediction->fresh()->points);
    }

    public function test_jackpot_algorithms_with_one_right_and_the_right_is_winner_team_predictions()
    {
        $stage = $this->createStage16Matches();

        /** @var Match $match */
        $match = Match::find(1);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $exactPrediction = $match->predict(1, 2);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $losePrediction = $match->predict(2, 1);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $diffPrediction = $match->predict(2, 3);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $winnerPrediction = $match->predict(1, 3);

        Match::find(1)->setTeamsIds(2, 4);
        $match->started_at = Match::markAsFinished();
        $match->finish(1, 2);
        $match->evaluate();

        $this->assertEquals(21.333333333334, $exactPrediction->fresh()->points);
        $this->assertEquals(16.0000000000005, $diffPrediction->fresh()->points);
        $this->assertEquals(10.666666666667, $winnerPrediction->fresh()->points);
        $this->assertEquals(-10, $losePrediction->fresh()->points);
    }

    public function test_jackpot_algorithms_with_one_right_and_the_right_is_loser_team_predictions()
    {
        $stage = $this->createStage16Matches();

        /** @var Match $match */
        $match = Match::find(1);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $exactPrediction = $match->predict(1, 2);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $losePrediction = $match->predict(2, 1);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $diffPrediction = $match->predict(2, 3);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $winnerPrediction = $match->predict(1, 3);

        Match::find(1)->setTeamsIds(1, 3);
        $match->started_at = Match::markAsFinished();
        $match->finish(1, 2);
        $match->evaluate();

        $this->assertEquals(21.333333333334, $exactPrediction->fresh()->points);
        $this->assertEquals(16.0000000000005, $diffPrediction->fresh()->points);
        $this->assertEquals(10.666666666667, $winnerPrediction->fresh()->points);
        $this->assertEquals(-10, $losePrediction->fresh()->points);
    }

    public function test_jackpot_algorithms_with_both_lose_team_predictions()
    {
        $stage = $this->createStage16Matches();

        /** @var Match $match */
        $match = Match::find(1);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $exactPrediction = $match->predict(1, 2);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $losePrediction = $match->predict(2, 1);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $diffPrediction = $match->predict(2, 3);

        $this->loginAsNewUser();
        StagePrediction::predict([$stage, $this->teamsOrders()]);
        $winnerPrediction = $match->predict(1, 3);

        Match::find(1)->setTeamsIds(2, 3);
        $match->started_at = Match::markAsFinished();
        $match->finish(1, 2);
        $match->evaluate();

        $this->assertEquals(21.333333333334, $exactPrediction->fresh()->points);
        $this->assertEquals(16.0000000000005, $diffPrediction->fresh()->points);
        $this->assertEquals(10.666666666667, $winnerPrediction->fresh()->points);
        $this->assertEquals(-10, $losePrediction->fresh()->points);
    }

    public function teamsOrders()
    {
        return [
            "A1" => 1, "A2" => 2, "B1" => 3, "B2" => 4, "C1" => 5, "C2" => 6, "D1" => 7, "D2" => 8,
            "E1" => 9, "E2" => 10, "F1" => 11, "F2" => 12, "G1" => 13, "G2" => 14, "H1" => 15, "H2" => 16,
        ];
    }
}
