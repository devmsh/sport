<?php

namespace Tests\Unit;

use App\Match;
use App\MatchTeam;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MatchTest extends TestCase
{
    use DatabaseMigrations;

    public function test_match_finished_after_three_matched_result()
    {
        $match = $this->createMatch(Carbon::yesterday());
        $this->loginAsNewUser();

        $this->loginAsNewUser();
        $match->addResult(1, 2);
        $this->loginAsNewUser();
        $match->addResult(1, 2);
        $match->addResult(1, 2);
        // must have 3 occurrence from different user
        $this->assertNull($match->fresh()->first_team_result);

        $this->loginAsNewUser();
        $match->addResult(1, 3);
        // must have 3 occurrence for the same result
        $this->assertNull($match->fresh()->first_team_result);

        $this->loginAsNewUser();
        $match->addResult(1, 2);
        $this->assertEquals(Match::STATUS_EVALUATED, $match->fresh()->status);
    }

    public function test_can_get_match_winner()
    {
        $match = $this->createMatch(Carbon::yesterday());

        $match->finish(1,2);
        $this->assertEquals($match->winner()->id,$match->second_team_id);

        $match->finish(2,1);
        $this->assertEquals($match->winner()->id,$match->first_team_id);

        $match->finish(2,2);
        $this->assertNull($match->winner());
    }

    public function test_can_get_match_loser()
    {
        $match = $this->createMatch(Carbon::yesterday());

        $match->finish(1,2);
        $this->assertEquals($match->loser()->id,$match->first_team_id);

        $match->finish(2,1);
        $this->assertEquals($match->loser()->id,$match->second_team_id);

        $match->finish(2,2);
        $this->assertNull($match->loser());
    }

    public function test_match_winner_can_be_auto_move_to_other_match_first()
    {
        $match = $this->createMatch(Carbon::yesterday());
        $otherMatch = $this->createLabeledMatch();

        $match->winner_to_id = $otherMatch->id;
        $match->winner_to_position = Match::POSITION_FIRST;

        $this->assertNull($otherMatch->first_team_id);
        $this->assertNull($otherMatch->second_team_id);

        $match->finish(1,2);

        $otherMatch = $otherMatch->fresh();
        $this->assertEquals($match->winner(), $otherMatch->firstTeam);
        $this->assertNull($otherMatch->second_team_id);
    }

    public function test_match_winner_can_be_auto_move_to_other_match_second()
    {
        $match = $this->createMatch(Carbon::yesterday());
        $otherMatch = $this->createLabeledMatch();

        $match->winner_to_id = $otherMatch->id;
        $match->winner_to_position = Match::POSITION_SECOND;

        $matchTeams = $otherMatch->teams;
        $this->assertNull($matchTeams->first_team_id);
        $this->assertNull($matchTeams->second_team_id);

        $match->finish(1,2);

        $matchTeams = $matchTeams->fresh();
        $this->assertEquals($match->winner(), $matchTeams->secondTeam);
        $this->assertNull($matchTeams->first_team_id);
    }

    public function test_match_loser_can_be_auto_move_to_other_match()
    {
        $match = $this->createMatch(Carbon::yesterday());
        $otherMatch = $this->createLabeledMatch();

        $match->loser_to_id = $otherMatch->id;
        $match->loser_to_position = Match::POSITION_SECOND;

        $matchTeams = $otherMatch->teams;
        $this->assertNull($matchTeams->first_team_id);
        $this->assertNull($matchTeams->second_team_id);

        $match->finish(1,2);

        $matchTeams = $matchTeams->fresh();
        $this->assertEquals($match->loser(), $matchTeams->secondTeam);
        $this->assertNull($matchTeams->first_team_id);
    }
}
