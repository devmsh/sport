<?php

namespace Tests;

use App\Classes\FacebookProvider;
use App\Match;
use App\MatchTeam;
use App\Season;
use App\Stage;
use App\StagePrediction;
use App\Team;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Cache;
use Laravel\Passport\Passport;
use Mockery;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public $provider;

    public function loginAsNewUser($withPoints = true, $extra = [])
    {
        $user = factory(User::class)->create($extra);
        if ($withPoints) {
            $user->addPoints(1000, "Registration points");
        }

        Passport::actingAs($user);

        return $user;
    }

    public function createMatches($number = 1, $date = null)
    {
        $date = $date ?? Carbon::tomorrow();
        $matches = factory(Match::class, $number)->create([
            'started_at' => $date
        ]);

        return $matches;
    }

    public function createLabeledMatches($number = 1, $date = null)
    {
        $date = $date ?? Carbon::tomorrow();
        $matches = factory(Match::class, $number)->create([
            'first_team_id' => null,
            'second_team_id' => null,
            'started_at' => $date
        ]);

        return $matches;
    }


    /**
     * @param null $date
     * @return Match
     */
    public function createMatch($date = null)
    {
        return $this->createMatches(1, $date)->first();
    }

    public function createLabeledMatch($date = null)
    {
        return $this->createLabeledMatches(1, $date)->first();
    }

    public function createGroup($number = 1, $season = null)
    {
        $groups = array_slice(range('A', 'Z'), 0, $number);

        $season = $season ?? factory(Season::class)->create();

        foreach ($groups as $group) {
            $season->teams()->attach(factory(Team::class, 4)->create(), [
                "group" => $group
            ]);
        }

        return $season;
    }

    public function createWorldCupMatches($season)
    {
//        foreach ($map as $match) {
//            factory(MatchTeam::class)->create([
//                'first_team_label' => $match[0],
//                'second_team_label' => $match[1],
//                'first_team_id' => null,
//                'second_team_id' => null,
//                'match_id' => factory(Match::class)->create([
//                    'season_id' => $season->id,
//                    'stage_id' => $stage->id
//                ])->id
//            ]);
//        }
    }

    public function createWorldCupStages($season = null)
    {
        $season = $season ?? factory(Season::class)->create();

        factory(Stage::class)->create([
            "name" => "Group Stage",
            "type" => Stage::STAGE_GROUPS,
            "season_id" => $season->id
        ]);

        factory(Stage::class)->create([
            "name" => "16 Stage",
            "type" => Stage::STAGE_16,
            "season_id" => $season->id
        ]);

        factory(Stage::class)->create([
            "name" => "1/4 final Stage",
            "type" => Stage::STAGE_8,
            "season_id" => $season->id
        ]);

        factory(Stage::class)->create([
            "name" => "1/2 final Stage",
            "type" => Stage::STAGE_4,
            "season_id" => $season->id
        ]);

        factory(Stage::class)->create([
            "name" => "3rd place Stage",
            "type" => Stage::STAGE_THIRD,
            "season_id" => $season->id
        ]);

        factory(Stage::class)->create([
            "name" => "Final Stage",
            "type" => Stage::STAGE_FINAL,
            "season_id" => $season->id
        ]);

        return $season;
    }

    public function createStageMatches($map, $season = null, $type)
    {
        $season = $season ?? factory(Season::class)->create();

        $stage = factory(Stage::class)->create(['name' => '16', 'type' => $type]);

        foreach ($map as $match) {
            factory(Match::class)->create([
                'first_team_label' => $match[0],
                'second_team_label' => $match[1],
                'first_team_id' => null,
                'second_team_id' => null,
                'season_id' => $season->id,
                'stage_id' => $stage->id
            ]);
        }

        return $stage;
    }

    public function createStage16Matches($season = null)
    {
        return $this->createStageMatches(Stage::STAGE_16_MAP, $season, Stage::STAGE_16);
    }

    public function createStage8Matches($season = null)
    {
        return $this->createStageMatches(Stage::STAGE_8_MAP, $season, Stage::STAGE_8);
    }

    public function mockFacebookSDK($fb_id = null)
    {
        $token = "fakeToken";
        $fbUser = new \stdClass();
        $fbUser->id = $fb_id ?? '822789892';
        $fbUser->name = 'Facebook User';
        $fbUser->email = 'example@facebook.com';
        $fbUser->access_token = $token;

        $this->provider = Mockery::mock(FacebookProvider::class);

        $this->app->instance(FacebookProvider::class, $this->provider);

        $this->provider->shouldReceive('getUser')->with($token)->andReturn($fbUser);
    }

    public function mockFacebookFriends($id = 1)
    {
        $friends = json_decode(file_get_contents(__DIR__ . "/stubs/friends-$id.json"), true);

        foreach ($friends['data'] as $friend) {
            factory(User::class)->create([
                'fb_id' => $friend['id']
            ]);
        }

        $this->provider->shouldReceive('getFriends')->andReturn($friends);
    }

    public function assertKeyCachedForever($key)
    {
        Cache::shouldReceive('rememberForever')->once()->with($key, \Mockery::any());
    }

    public function assertKeyCached($key)
    {
        Cache::shouldReceive('remember')->once()->with($key, \Mockery::any(), \Mockery::any());
    }
}
