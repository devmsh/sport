<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $guarded = [];

    protected $with = ['favoritable'];

    protected static $map = [
        "league" => \App\League::class,
        "team" => \App\Team::class,
        "match" => \App\Match::class,
    ];

    public function favoritable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function toggle(User $user, $type, $id)
    {
        $model = array_get(self::$map, $type);

        $object = $model::findOrFail($id);

        $favorite = Favorite::firstOrNew([
            'user_id' => $user->id,
            'favoritable_type' => get_class($object),
            'favoritable_id' => $object->id,
        ]);

        if ($favorite->exists) {
            $favorite->delete();
            $object->decrement('followers_count');
            return false;
        } else {
            $favorite->save();
            $object->increment('followers_count');
            return true;
        }
    }

    public function toArray()
    {
        return array_merge(parent::toArray(), [
            'favoritable_type' => strtolower(str_replace("App\\", "", $this->favoritable_type))
        ]);
    }
}
