<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class MatchIdsSet
{
    use SerializesModels;
    public $matchTeam;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($matchTeam)
    {
        $this->matchTeam = $matchTeam;
    }
}