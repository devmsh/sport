<?php

namespace App\Events;

use App\Match;
use Illuminate\Queue\SerializesModels;

class MatchFinished
{
    use SerializesModels;

    /** @var Match $match */
    public $match;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($match)
    {
        $this->match = $match;
    }
}
