<?php

namespace App\Rules;

use App\Match;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class IsHaveLessThan implements Rule
{
    protected $period;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($period)
    {
        $this->period = $period;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $match)
    {
        if (!$match || $match->started_at > Carbon::now()->addDays($this->period))
            return false;
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You can\'t predict match will start after ' . $this->period . ' days';
    }
}
