<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class FavoriteableExist implements Rule
{
    protected $type;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $map = [
            "league" => \App\League::class,
            "team" => \App\Team::class,
            "match" => \App\Match::class,
        ];

        $model = array_get($map, $this->type);

        return $model::find($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid Type';
    }
}
