<?php

namespace App\Nova;

use App\Nova\Actions\AttachToSeason;
use Devmsh\ImageCropper\ImageCropper;
use Digitalcloud\MultilingualNova\Multilingual;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;
use MrMonat\Translatable\Translatable;

class Team extends Resource
{
    public static $with = ['seasons'];
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Team';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Avatar::make('logo')->thumbnail(function () {
                return $this->logo;
            }),
            Text::make('name'),
            Text::make('shortcut'),
            Number::make('Followers count')->onlyOnIndex()->sortable(),
            BelongsToMany::make('seasons')->fields(function () {
                return [
                    Text::make('group'),
                ];
            })->searchable(),
            HasMany::make('homeMatches', 'homeMatches', Match::class),
            HasMany::make('awayMatches', 'awayMatches', Match::class),
            Multilingual::make('Translations')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new AttachToSeason
        ];
    }
}
