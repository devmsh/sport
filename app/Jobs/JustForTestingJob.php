<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mockery\Exception;

class JustForTestingJob implements ShouldQueue {

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($name) {
        //
        $this->name = $name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        if ($this->name == "F") {
            throw new Exception('F is not a name');
        }
    }
}
