<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Spatie\Translatable\HasTranslations;

class Country extends Model {

    use HasTranslations;

    public $translatable = ['name'];

    protected $guarded = [];

    public function getLogoAttribute($value)
    {
        return url($value);
    }

    public function setLogoAttribute($value)
    {
        if ($value == null) {
            $this->attributes["logo"] = null;
        }

        if (starts_with($value, 'data:image')) {
            $filename = $this->getRoundedLogo($value);
            $this->attributes["logo"] = $filename;
        }else{
            $this->attributes["logo"] = $value;
        }
    }

    public function getRoundedLogo($value)
    {
        $img = Image::make($value)->resize(200, 200);

        $width = $img->getWidth();
        $height = $img->getHeight();

        $mask = Image::canvas($width, $height);

        // draw a white circle
        $mask->circle($width, $width / 2, $height / 2, function ($draw) {
            $draw->background('#fff');
        });

        $img->mask($mask, true);
        $img->resize(100, 100);

        $file_name = "uploads/countries/{$this->id}.png";
        $img->save(public_path($file_name));

        return $file_name;
    }
}
