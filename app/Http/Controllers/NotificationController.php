<?php

namespace App\Http\Controllers;

use App\Leaderboard;
use App\MatchResultPrediction;
use App\Notifications\FCMNotification;
use App\Stage;
use App\Team;
use App\User;
use Illuminate\Http\Request;
use LaravelFCM\Response\DownstreamResponse;

class NotificationController extends Controller
{
    public function notify($type = null)
    {
        if (!$type) {
            echo "<p>userWithoutPredictions</p>";
            echo "<p>followedTeamsNotification</p>";
            echo "<p>followedTeamsNotification</p>";
            echo "<p>userWithoutPredictions</p>";
            echo "<p>drawPredictionProblem</p>";
            echo "<p>coaches</p>";
        } else {
            /** @var DownstreamResponse $response */
            $response = $this->$type();

            if ($response) {
                var_dump($response->numberSuccess());
                var_dump($response->numberFailure());
                var_dump($response->numberModification());
                var_dump($response->tokensToModify());
                var_dump($response->tokensToDelete());
                var_dump($response->tokensToRetry());
                return $response->tokensWithError();
            }
        }
    }

    public function leaderboard()
    {
        $leaders = Leaderboard::top(50, Stage::find(2));

        $title = "Round of 16 leaderboard";

        foreach ($leaders as $leader) {
            $userId = $leader['id'];
            $body = "You ranked as #{$leader['rank']} on this stage, go on!";
            $user = \App\User::find($userId);
            $tokens = collect([$user->device_token]);

            /** @var DownstreamResponse $response */
            $response = FCMNotification::simpleNotify($tokens, $title, $body);
            var_dump($response->numberSuccess() ? "$userId success" : "$userId failed");
        }
    }

    public function coaches()
    {
        $teams = Team::where('id','>',65)->get();
        foreach ($teams as $team) {
            if(!$team->coach_id) continue;
            $coach = User::find($team->coach_id);
            $secondCoach = User::find($team->second_coach_id);
            $thirdCoach = User::find($team->third_coach_id);

            $title = "Congrats, you become {$team->name} coach";
            $body = "Predict next matches and keep your position";
            $response = FCMNotification::simpleNotify(collect([$coach->device_token]), $title, $body);
            var_dump($response->numberSuccess() ? "{$coach->name} {$coach->id} success" : "{$coach->name} {$coach->id} failed");

            $title = "Congrats, you become {$team->name} coach assistance";
            $body = "Predict next matches and keep your position";
            $response = FCMNotification::simpleNotify(collect([$secondCoach->device_token]), $title, $body);
            var_dump($response->numberSuccess() ? "{$secondCoach->name} {$secondCoach->id} success" : "{$secondCoach->name} {$secondCoach->id} failed");

            $title = "Congrats, you become {$team->name} coach assistance";
            $body = "Predict next matches and keep your position";
            $response = FCMNotification::simpleNotify(collect([$thirdCoach->device_token]), $title, $body);
            var_dump($response->numberSuccess() ? "{$thirdCoach->name} {$thirdCoach->id} success" : "{$thirdCoach->name} {$thirdCoach->id} failed");
        }
    }

    public function result()
    {
        $users = \App\User::pluck('device_token');
        $title = "Croatia vs Denmark";
        $body = "First half ends with draw, Croatia 1:1 Denmark";
        /** @var DownstreamResponse $response */
        $response = FCMNotification::simpleNotify($users, $title, $body);
        return $response;
    }

    public function userWithoutPredictions()
    {
        $users = \App\User::where('points', '1000')->get();
        $title = "You have only 1 hour!";
        $body = "Share your semi-finals predictions";
        /** @var DownstreamResponse $response */
        $response = FCMNotification::simpleNotify($users->pluck('device_token'), $title, $body);
        return $response;
    }

    public function drawPredictionProblem()
    {
        $userIds = MatchResultPrediction::whereHas('match.stage', function ($query) {
            $query->where('can_draw', 0);
        })->whereRaw('first_team_result = second_team_result')->pluck('user_id');

        $tokens = \App\User::whereIn('id', $userIds)->pluck('device_token');

        $title = "No draw in 16 round";
        $body = "Please update your prediction and select a winner";
        $response = FCMNotification::simpleNotify($tokens, $title, $body);
        return $response;
    }

    public function unCompleteRegistration(): array
    {
        $users = \App\User::where('is_completed', false)->get();
        $title = "Hurry up!";
        $body = "Complete your registration and get 1000 points";
        /** @var DownstreamResponse $response */
        $response = FCMNotification::simpleNotify($users->pluck('device_token'), $title, $body);
        return $response;
    }

    public function followedTeamsNotification()
    {
        $matches = \App\Match::where('started_at', '>', \Carbon\Carbon::now())
            ->where('started_at', '<', \Carbon\Carbon::now()->addDays(2))
            ->get();

        $followers = collect([]);
        foreach ($matches as $match) {
            $firstTeam = $match->teams->firstTeam;
            $secondTeam = $match->teams->secondTeam;

            $matchFollowers = collect([]);
            $matchFollowers = $matchFollowers->merge(\App\Favorite::where('favoritable_type', \App\Team::class)
                ->where('favoritable_id', $firstTeam->id)->get(['user_id', 'favoritable_id']));

            $matchFollowers = $matchFollowers->merge(\App\Favorite::where('favoritable_type', \App\Team::class)
                ->where('favoritable_id', $secondTeam->id)->get(['user_id', 'favoritable_id']));

            $predictedFollowers = \App\MatchResultPrediction::where('match_id', $match->id)
                ->whereIn('user_id', $matchFollowers->pluck('user_id'))
                ->pluck('user_id');

            $matchFollowers = $matchFollowers->whereNotIn('user_id', $predictedFollowers);

            $followers = $followers->merge($matchFollowers);
        }

        $followers = $followers->groupBy('user_id')->map(function ($teams) {
            return $teams->pluck('favoritable_id')->unique()->take(2)->map(function ($id) {
                return Team::find($id)->name;
            });
        });

        foreach ($followers as $userId => $teams) {
            $user = \App\User::find($userId);
            if ($user->points == 1000) continue;
            $tokens = collect([$user->device_token]);
            $teams = implode(" & ", $teams->all());
            $title = "Favorite teams";
            $body = "Go and predict $teams matches";

            /** @var DownstreamResponse $response */
            $response = FCMNotification::simpleNotify($tokens, $title, $body);
            var_dump($response->numberSuccess() ? "$userId success" : "$userId failed");
        }
    }
}
