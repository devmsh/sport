<?php

namespace App\Http\Controllers\API;

use App\Association;
use App\Http\Controllers\Controller;
use App\League;
use Illuminate\Support\Facades\Cache;

class LeagueController extends Controller
{
    public function index()
    {
//        return Cache::remember('leagues', 1440, function () {
            return Association::with('leagues')
                ->where('id','!=',3)
                ->get();
//        });
    }

    public function show(League $league)
    {
//        return Cache::remember("league-{$league->id}", 1440, function () use ($league) {
            return $league;
//        });
    }
}
