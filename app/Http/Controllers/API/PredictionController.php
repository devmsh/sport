<?php

namespace App\Http\Controllers\API;

use App\MatchResultPrediction;
use App\MatchTeamPrediction;
use App\Models\Match;
use App\Repositories\MatchesRepository;
use App\Season;
use App\StagePrediction;
use App\Team;
use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PredictionController extends Controller
{
    public function __construct()
    {
        if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {
            $this->middleware('auth:api');
        }
    }

    public function matches(Request $request)
    {
        $date = request('started_at', Carbon::today()->format("d-m-Y"));
        $date = $this->indicToArabic($date);

        return [
            "matches" => MatchesRepository::getPredictions($date, $request, request('isPredict', false)),
//            "teams" => MatchTeamPrediction::getPredictions($date, $request, auth("api")->id(), request('isPredict', false))
        ];
    }

    public function stages(Season $season)
    {
        return StagePrediction::with('stage')->whereIn('stage_id', $season->stages()->pluck('id'))
            ->where('user_id', auth('api')->id())
            ->get();
    }

    public function image16($id)
    {
        /** @var \Intervention\Image\Image $true */
        $true = Image::make(storage_path('true.png'))->resize(15, 15);
        /** @var \Intervention\Image\Image $false */
        $false = Image::make(storage_path('false.png'))->resize(15, 15);
        $boolImages = [
            true => $true,
            false => $false
        ];

        /** @var \Intervention\Image\Image $img */
        $img = Image::make(storage_path('facebook_full.png'));

        $user = User::find($id);
        $userName = $user->name;

        $red = '#C82C31';
        $green = '#6EC742';
        $map = [
            "A1" => [70, 64, 34, 54],
            "B2" => [70, 136, 34, 126],
            "C1" => [70, 223, 34, 213],
            "D2" => [70, 295, 34, 285],
            "E1" => [70, 382, 34, 372],
            "F2" => [70, 454, 34, 444],
            "G1" => [70, 541, 34, 531],
            "H2" => [70, 613, 34, 603],

            "B1" => [845, 64, 904, 54],
            "A2" => [845, 136, 904, 126],
            "D1" => [845, 223, 904, 213],
            "C2" => [845, 295, 904, 285],
            "F1" => [845, 382, 904, 372],
            "E2" => [845, 454, 904, 444],
            "H1" => [845, 541, 904, 531],
            "G2" => [845, 613, 904, 603],

            "W49" => [204, 88, 176, 76],
            "W50" => [204, 271, 176, 323],
            "W53" => [204, 406, 176, 394],
            "W54" => [204, 589, 176, 641],

            "W51" => [712, 88, 684, 76],
            "W52" => [712, 271, 684, 323],
            "W55" => [712, 406, 684, 394],
            "W56" => [712, 589, 684, 641],

            "W57" => [299, 168, 271, 156],
            "W58" => [299, 510, 271, 562],
            "W60" => [617, 168, 589, 156],
            "W59" => [617, 510, 589, 562],

            "W61" => [365, 299, 324, 289],
            "W62" => [550, 299, 614, 289],

            "L61" => [414, 505, 385, 493],
            "L62" => [509, 505, 480, 493],
        ];

        $watermark = Image::make(public_path("uploads/0.png"))->resize(25, 25);
        foreach ($map as $key => [$x, $y]) {
            if ($key == "L61" || $key == "L62") {
                $img->insert($watermark->resize(18, 18), 'top-left', $x, $y);
                $img->text($key, $x + 9, $y + 35, function ($font) {
                    $font->file(storage_path('Avenir-Roman.ttf'));
                    $font->size(11);
                    $font->color('#333333');
                    $font->align('center');
                    $font->valign('center');
                });
            } else {
                $img->insert($watermark, 'top-left', $x, $y);
                $img->text($key, $x + 12, $y + 40, function ($font) {
                    $font->file(storage_path('Avenir-Roman.ttf'));
                    $font->size(13);
                    $font->color('#333333');
                    $font->align('center');
                    $font->valign('center');
                });
            }
        }

        $predictions = MatchTeamPrediction::where('user_id', $user->id)->with('match')->whereHas('match', function ($query) {
            $query->where('stage_id', 2);
        })->get();

        $data = [];
        foreach ($predictions as $prediction) {
            $matchResultPrediction = MatchResultPrediction::firstOrNew([
                'user_id' => $user->id,
                'match_id' => $prediction->match_id
            ]);

            if ($prediction->first_team_id) {
                if ($prediction->match->teams->firstTeam) {
                    $team = $prediction->match->teams->firstTeam;
                } else {
                    $team = Team::find($prediction->first_team_id);
                }
                $teamLogo = Image::make(storage_path("teams/{$team->id}.png"));
                $color = $matchResultPrediction->first_team_result > $matchResultPrediction->second_team_result ? $green : $red;
                $data [] = array_flatten([$teamLogo->resize(25, 25), $team->name, $matchResultPrediction->first_team_result, $map[$prediction->match->teams->first_team_label], $color]);
            }

            if ($prediction->second_team_id) {
                if ($prediction->match->teams->secondTeam) {
                    $team = $prediction->match->teams->secondTeam;
                } else {
                    $team = Team::find($prediction->second_team_id);
                }
                $color = $color == $green ? $red : $green;
                $teamLogo = Image::make(storage_path("teams/{$team->id}.png"));
                $data [] = array_flatten([$teamLogo->resize(25, 25), $team->name, $matchResultPrediction->second_team_result, $map[$prediction->match->teams->second_team_label], $color]);
            }


            [$x, $y] = $map[$prediction->match->teams->first_team_label];
            if ($x < 845) {
                $theX = $x + 90;
            } else {
                $theX = $x - 80;
            }
            if ($matchResultPrediction->match->winner()) {
                $bool = $matchResultPrediction->winner() == $matchResultPrediction->match->winner();
                $img->insert($boolImages[$bool], 'top-left', $theX, $y + 50);
            }
        }

        foreach ($data as $key => [$watermark, $team, $teamResult, $x, $y, $rx, $ry, $color]) {
            $img->insert($watermark, 'top-left', $x, $y);
            $img->rectangle($rx, $ry, $rx + 1, $ry + 62, function ($draw) use ($color) {
                $draw->background($color);
            });
            $img->rectangle($x - 10, $y + 30, $x + 30, $y + 50, function ($draw) use ($color) {
                $draw->background("#FFFFFF");
            });
            $img->text($team, $x + 15, $y + 40, function ($font) {
                $font->file(storage_path('Avenir-Roman.ttf'));
                $font->size(13);
                $font->color('#333333');
                $font->align('center');
                $font->valign('center');
            });
            if ($x < 845) {
                $theX = $x + 73;
            } else {
                $theX = $x - 50;
            }
            $img->text($teamResult, $theX, $y + 20, function ($font) {
                $font->file(storage_path('Avenir-Roman.ttf'));
                $font->size(16);
                $font->color('#333333');
                $font->align('center');
                $font->valign('center');
            });
        }

//        $data = [
//            [$watermark, $team, $teamResult, 204, 88, 176, 76, $red],
//            [$watermark, $team, $teamResult, 204, 271, 176, 323, $green],
//            [$watermark, $team, $teamResult, 204, 406, 176, 394, $red],
//            [$watermark, $team, $teamResult, 204, 589, 176, 641, $green],
//
//            [$watermark, $team, $teamResult, 712, 88, 684, 76, $red],
//            [$watermark, $team, $teamResult, 712, 271, 684, 323, $green],
//            [$watermark, $team, $teamResult, 712, 406, 684, 394, $red],
//            [$watermark, $team, $teamResult, 712, 589, 684, 641, $green],
//        ];
        $predictions = MatchTeamPrediction::where('user_id', $user->id)->with('match')->whereHas('match', function ($query) {
            $query->where('stage_id', 3);
        })->get();

        $data = [];
        foreach ($predictions as $prediction) {
            $matchResultPrediction = MatchResultPrediction::firstOrNew([
                'user_id' => $user->id,
                'match_id' => $prediction->match_id
            ]);

            if ($prediction->first_team_id) {
                if ($prediction->match->teams->firstTeam) {
                    $team = $prediction->match->teams->firstTeam;
                } else {
                    $team = Team::find($prediction->first_team_id);
                }
                $teamLogo = Image::make(storage_path("teams/{$team->id}.png"));
                $color = $matchResultPrediction->first_team_result > $matchResultPrediction->second_team_result ? $green : $red;
                $data [] = array_flatten([$teamLogo->resize(25, 25), $team->name, $matchResultPrediction->first_team_result, $map[$prediction->match->teams->first_team_label], $color]);
            }

            if ($prediction->second_team_id) {
                if ($prediction->match->teams->secondTeam) {
                    $team = $prediction->match->teams->secondTeam;
                } else {
                    $team = Team::find($prediction->second_team_id);
                }
                $color = $color == $green ? $red : $green;
                $teamLogo = Image::make(storage_path("teams/{$team->id}.png"));
                $data [] = array_flatten([$teamLogo->resize(25, 25), $team->name, $matchResultPrediction->second_team_result, $map[$prediction->match->teams->second_team_label], $color]);
            }

            [$x, $y] = $map[$prediction->match->teams->first_team_label];
            if ($matchResultPrediction->match->winner()) {
                $bool = $matchResultPrediction->winner() == $matchResultPrediction->match->winner();
                $img->insert($boolImages[$bool], 'top-left', $x + 5, $y + 105);
            }
        }

        foreach ($data as $key => [$watermark, $team, $teamResult, $x, $y, $rx, $ry, $color]) {
            $img->insert($watermark, 'top-left', $x, $y);
            $img->rectangle($rx, $ry, $rx + 80, $ry + 1, function ($draw) use ($color) {
                $draw->background($color);
            });
            $img->rectangle($x - 10, $y + 30, $x + 30, $y + 50, function ($draw) use ($color) {
                $draw->background("#FFFFFF");
            });

            $img->text($team, $x + 12, $y + 40, function ($font) {
                $font->file(storage_path('Avenir-Roman.ttf'));
                $font->size(13);
                $font->color('#333333');
                $font->align('center');
                $font->valign('center');
            });
            if (($key + 1) % 2) {
                $theY = $y + 67;
            } else {
                $theY = $y - 25;
            }
            $img->text($teamResult, $x + 12.5, $theY, function ($font) {
                $font->file(storage_path('Avenir-Roman.ttf'));
                $font->size(16);
                $font->color('#333333');
                $font->align('center');
                $font->valign('center');
            });
        }

//        $data = [
//            [$watermark, $team, $teamResult, 299, 168, 271, 156, $red],
//            [$watermark, $team, $teamResult, 299, 510, 271, 562, $green],
//
//            [$watermark, $team, $teamResult, 617, 168, 589, 156, $red],
//            [$watermark, $team, $teamResult, 617, 510, 589, 562, $green],
//        ];
        $predictions = MatchTeamPrediction::where('user_id', $user->id)->with('match')->whereHas('match', function ($query) {
            $query->where('stage_id', 4);
        })->get();

        $data = [];
        foreach ($predictions as $prediction) {
            $matchResultPrediction = MatchResultPrediction::firstOrNew([
                'user_id' => $user->id,
                'match_id' => $prediction->match_id
            ]);

            if ($prediction->first_team_id) {
                if ($prediction->match->teams->firstTeam) {
                    $team = $prediction->match->teams->firstTeam;
                } else {
                    $team = Team::find($prediction->first_team_id);
                }
                $teamLogo = Image::make(storage_path("teams/{$team->id}.png"));
                $color = $matchResultPrediction->first_team_result > $matchResultPrediction->second_team_result ? $green : $red;
                $data [] = array_flatten([$teamLogo->resize(25, 25), $team->name, $matchResultPrediction->first_team_result, $map[$prediction->match->teams->first_team_label], $color]);
            }

            if ($prediction->second_team_id) {
                if ($prediction->match->teams->secondTeam) {
                    $team = $prediction->match->teams->secondTeam;
                } else {
                    $team = Team::find($prediction->second_team_id);
                }
                $color = $color == $green ? $red : $green;
                $teamLogo = Image::make(storage_path("teams/{$team->id}.png"));
                $data [] = array_flatten([$teamLogo->resize(25, 25), $team->name, $matchResultPrediction->second_team_result, $map[$prediction->match->teams->second_team_label], $color]);
            }
        }

        foreach ($data as $key => [$watermark, $team, $teamResult, $x, $y, $rx, $ry, $color]) {
            $img->insert($watermark, 'top-left', $x, $y);
            $img->rectangle($rx, $ry, $rx + 79, $ry + 1, function ($draw) use ($color) {
                $draw->background($color);
            });
            $img->rectangle($x - 10, $y + 30, $x + 30, $y + 50, function ($draw) use ($color) {
                $draw->background("#FFFFFF");
            });

            $img->text($team, $x + 12, $y + 40, function ($font) {
                $font->file(storage_path('Avenir-Roman.ttf'));
                $font->size(13);
                $font->color('#333333');
                $font->align('center');
                $font->valign('center');
            });
            if ($y < 200) {
                $theY = $y + 67;
            } else {
                $theY = $y - 25;
            }
            $img->text($teamResult, $x + 12.5, $theY, function ($font) {
                $font->file(storage_path('Avenir-Roman.ttf'));
                $font->size(16);
                $font->color('#333333');
                $font->align('center');
                $font->valign('center');
            });
        }

        //        $data = [
//            [$watermark, $team, $teamResult, 365, 299, 324, 289, $red],
//            [$watermark, $team, $teamResult, 550, 299, 614, 289, $green],
//        ];

        $predictions = MatchTeamPrediction::where('user_id', $user->id)->with('match')->whereHas('match', function ($query) {
            $query->where('stage_id', 6);
        })->get();

        $data = [];
        $winner = null;
        foreach ($predictions as $prediction) {
            $matchResultPrediction = MatchResultPrediction::firstOrNew([
                'user_id' => $user->id,
                'match_id' => $prediction->match_id
            ]);

            if ($prediction->first_team_id) {
                if ($prediction->match->teams->firstTeam) {
                    $team = $prediction->match->teams->firstTeam;
                } else {
                    $team = Team::find($prediction->first_team_id);
                }
                $teamLogo = Image::make(storage_path("teams/{$team->id}.png"));
                $color = $matchResultPrediction->first_team_result > $matchResultPrediction->second_team_result ? $green : $red;
                $data [] = array_flatten([$teamLogo->resize(25, 25), $team->name, $matchResultPrediction->first_team_result, $map[$prediction->match->teams->first_team_label], $color]);
            }
            if ($prediction->second_team_id) {
                if ($prediction->match->teams->secondTeam) {
                    $team = $prediction->match->teams->secondTeam;
                } else {
                    $team = Team::find($prediction->second_team_id);
                }
                $color = $color == $green ? $red : $green;
                $teamLogo = Image::make(storage_path("teams/{$team->id}.png"));
                $data [] = array_flatten([$teamLogo->resize(25, 25), $team->name, $matchResultPrediction->second_team_result, $map[$prediction->match->teams->second_team_label], $color]);
                if ($color == $green) {
                    $winner = last($data);
                }
            }
        }

        foreach ($data as $key => [$watermark, $team, $teamResult, $x, $y, $rx, $ry, $color]) {
            $img->insert($watermark, 'top-left', $x, $y);
            $img->rectangle($rx, $ry, $rx + 1, $ry + 62, function ($draw) use ($color) {
                $draw->background($color);
            });
            $img->rectangle($x - 10, $y + 30, $x + 30, $y + 50, function ($draw) use ($color) {
                $draw->background("#FFFFFF");
            });

            $img->text($team, $x + 12, $y + 40, function ($font) {
                $font->file(storage_path('Avenir-Roman.ttf'));
                $font->size(13);
                $font->color('#333333');
                $font->align('center');
                $font->valign('center');
            });
            if ($key < 1) {
                $theX = $x + 73;
            } else {
                $theX = $x - 50;
            }
            $img->text($teamResult, $theX, $y + 20, function ($font) {
                $font->file(storage_path('Avenir-Roman.ttf'));
                $font->size(16);
                $font->color('#333333');
                $font->align('center');
                $font->valign('center');
            });
        }


        if ($winner) {
            [$watermark, $team] = $winner;
        } else {
            $watermark = Image::make(public_path("uploads/0.png"))->resize(25, 25);
            $team = "WINNER";
        }
        $img->insert($watermark, 'top-left', 505, 371);
        $img->text($team, 505 + 12, 371 + 40, function ($font) {
            $font->file(storage_path('Avenir-Roman.ttf'));
            $font->size(13);
            $font->color('#333333');
            $font->align('center');
            $font->valign('center');
        });

//        $data = [
//            [$watermark, $team, $teamResult, 414, 505, 385, 493, $red],
//            [$watermark, $team, $teamResult, 509, 505, 480, 493, $green],
//        ];

        $predictions = MatchTeamPrediction::where('user_id', $user->id)->with('match')->whereHas('match', function ($query) {
            $query->where('stage_id', 5);
        })->get();

        $data = [];
        foreach ($predictions as $prediction) {
            $matchResultPrediction = MatchResultPrediction::firstOrNew([
                'user_id' => $user->id,
                'match_id' => $prediction->match_id
            ]);

            if ($prediction->first_team_id) {
                if ($prediction->match->teams->firstTeam) {
                    $team = $prediction->match->teams->firstTeam;
                } else {
                    $team = Team::find($prediction->first_team_id);
                }
                $teamLogo = Image::make(storage_path("teams/{$team->id}.png"));
                $color = $matchResultPrediction->first_team_result > $matchResultPrediction->second_team_result ? $green : $red;
                $data [] = array_flatten([$teamLogo->resize(25, 25), $team->name, $matchResultPrediction->first_team_result, $map[$prediction->match->teams->first_team_label], $color]);
            }

            if ($prediction->second_team_id) {
                if ($prediction->match->teams->secondTeam) {
                    $team = $prediction->match->teams->secondTeam;
                } else {
                    $team = Team::find($prediction->second_team_id);
                }
                $color = $color == $green ? $red : $green;
                $teamLogo = Image::make(storage_path("teams/{$team->id}.png"));
                $data [] = array_flatten([$teamLogo->resize(25, 25), $team->name, $matchResultPrediction->second_team_result, $map[$prediction->match->teams->second_team_label], $color]);
            }
        }

        foreach ($data as [$watermark, $team, $teamResult, $x, $y, $rx, $ry, $color]) {
            $watermark->resize(18, 18);
            $img->insert($watermark, 'top-left', $x, $y);
            $img->rectangle($rx, $ry, $rx + 75, $ry + 1, function ($draw) use ($color) {
                $draw->background($color);
            });
            $img->rectangle($x - 10, $y + 30, $x + 30, $y + 40, function ($draw) use ($color) {
                $draw->background("#FFFFFF");
            });

            $img->text($team, $x + 9, $y + 35, function ($font) {
                $font->file(storage_path('Avenir-Roman.ttf'));
                $font->size(11);
                $font->color('#333333');
                $font->align('center');
                $font->valign('center');
            });
            $img->text($teamResult, $x + 8, $y + 55, function ($font) {
                $font->file(storage_path('Avenir-Roman.ttf'));
                $font->size(15);
                $font->color('#333333');
                $font->align('center');
                $font->valign('center');
            });
        }

        $img->insert($this->getUserPhoto($user), 'top-left', 437, 119);
        $img->text($userName, 472, 220, function ($font) {
            $font->file(storage_path('Avenir-Roman.ttf'));
            $font->size(21);
            $font->color('#DEAB46');
            $font->align('center');
            $font->valign('center');
        });

        return $img->response('png');
    }

    public function image($id)
    {
        $prediction = MatchResultPrediction::findOrFail($id);

        $match = $prediction->match;
        $date = $match->started_at;
        $first_team = $match->teams->firstTeam;
        $first_team_prediction = $prediction->first_team_result;

        $second_team = $match->teams->secondTeam;
        $second_team_prediction = $prediction->second_team_result;
        $league = $match->season->league;

        if (!$first_team) {
            $userTeamPrediction = $match->userTeamPrediction(User::find($prediction->user_id))->first();
            $first_team = $userTeamPrediction->firstTeam;
            $second_team = $userTeamPrediction->secondTeam;
        }

        $path = "storage/predictions/{$match->id}_{$first_team_prediction}_{$second_team_prediction}.jpg";
        $full_path = public_path($path);

        if (file_exists($full_path)) {
            return redirect(url($path));
        }

        $img = Image::make(storage_path('facebook.png'));

        $watermark = Image::make(public_path("uploads/leagues/{$league->id}.png"));
        $img->insert($watermark->resize(120,120), 'top-left', 410, 120);
        $img->text($prediction->match->season->name, 470, 260, function ($font) {
            $font->file(storage_path('Avenir-Roman.ttf'));
            $font->size(45);
            $font->color('#333333');
            $font->align('center');
            $font->valign('top');
        });

        $img->text($date->format("D d, M - H:00") . " (UTC)", 470, 320, function ($font) {
            $font->file(storage_path('Avenir-Roman.ttf'));
            $font->size(22);
            $font->color('#333333');
            $font->align('center');
            $font->valign('top');
        });

        $watermark = Image::make(public_path("uploads/teams/{$first_team->id}.png"));
        $img->insert($watermark, 'top-left', 195, 385);
        $img->text($first_team->name, 240, 510, function ($font) {
            $font->file(storage_path('Avenir-Roman.ttf'));
            $font->size(30);
            $font->color('#333333');
            $font->align('center');
            $font->valign('top');
        });
        $img->text($first_team_prediction, 430, 420, function ($font) {
            $font->file(storage_path('Avenir-Roman.ttf'));
            $font->size(45);
            $font->color('#FFFFFF');
            $font->align('center');
            $font->valign('top');
        });

        $watermark = Image::make(public_path("uploads/teams/{$second_team->id}.png"));
        $img->insert($watermark, 'top-right', 195, 385);
        $img->text($second_team->name, 940 - 240, 510, function ($font) {
            $font->file(storage_path('Avenir-Roman.ttf'));
            $font->size(30);
            $font->color('#333333');
            $font->align('center');
            $font->valign('top');
        });
        $img->text($second_team_prediction, 940 - 430, 420, function ($font) {
            $font->file(storage_path('Avenir-Roman.ttf'));
            $font->size(45);
            $font->color('#FFFFFF');
            $font->align('center');
            $font->valign('top');
        });

        $img->save($full_path);

        return redirect(url($path));
    }

    function indicToArabic($str)
    {
        $arabic = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $indic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        return str_replace($indic, $arabic, $str);
    }

    public function getRoundedLogo($team)
    {
        $logo = $team->logo;
        $img = Image::make($logo)->resize(200, 200);

        $width = $img->getWidth();
        $height = $img->getHeight();

        $mask = \Image::canvas($width, $height);

        // draw a white circle
        $mask->circle($width, $width / 2, $height / 2, function ($draw) {
            $draw->background('#fff');
        });

        $img->mask($mask, true);
        $img->resize(100, 100);

        $img->save(storage_path("teams/{$team->id}.png"));

        return null;
    }

    public function getUserPhoto($user)
    {
        $logo = $user->photo;
        $img = Image::make($logo);

        $width = $img->getWidth();
        $height = $img->getHeight();

        $mask = \Image::canvas($width, $height);

        // draw a white circle
        $mask->circle($width, $width / 2, $height / 2, function ($draw) {
            $draw->background('#fff');
        });

        $img->mask($mask, true);
        $img->resize(67, 67);

        return $img;
    }

    /**
     * word2uni
     * This code is a part of aCAPTCHA project, This copyright notice MUST stay intact for use
     * @package aCAPTCHA
     * @author Abd Allatif Eymsh
     * @copyright (c) 2012
     * @param string $word
     * @license http://opensource.org/licenses/gpl-license.php GNU General Public License v2
     */
    function word2uni($word)
    {

        $new_word = array();
        $char_type = array();
        $isolated_chars = array('ا', 'د', 'ذ', 'أ', 'آ', 'ر', 'ؤ', 'ء', 'ز', 'و', 'ى', 'ة');

        $all_chars = array
        (
            'ا' => array(

                'middle' => '&#xFE8E;',

                'isolated' => '&#xFE8D;'
            ),

            'ؤ' => array(

                'middle' => '&#xFE85;',

                'isolated' => '&#xFE86;'
            ),
            'ء' => array(
                'middle' => '&#xFE80;',
                'isolated' => '&#xFE80;'
            ),
            'أ' => array(

                'middle' => '&#xFE84;',

                'isolated' => '&#xFE83;'
            ),
            'آ' => array(

                'middle' => '&#xFE82;',

                'isolated' => '&#xFE81;'
            ),
            'ى' => array(

                'middle' => '&#xFEF0;',

                'isolated' => '&#xFEEF;'
            ),
            'ب' => array(
                'beginning' => '&#xFE91;',
                'middle' => '&#xFE92;',
                'end' => '&#xFE90;',
                'isolated' => '&#xFE8F;'
            ),
            'ت' => array(
                'beginning' => '&#xFE97;',
                'middle' => '&#xFE98;',
                'end' => '&#xFE96;',
                'isolated' => '&#xFE95;'
            ),
            'ث' => array(
                'beginning' => '&#xFE9B;',
                'middle' => '&#xFE9C;',
                'end' => '&#xFE9A;',
                'isolated' => '&#xFE99;'
            ),
            'ج' => array(
                'beginning' => '&#xFE9F;',
                'middle' => '&#xFEA0;',
                'end' => '&#xFE9E;',
                'isolated' => '&#xFE9D;'
            ),
            'ح' => array(
                'beginning' => '&#xFEA3;',
                'middle' => '&#xFEA4;',
                'end' => '&#xFEA2;',
                'isolated' => '&#xFEA1;'
            ),
            'خ' => array(
                'beginning' => '&#xFEA7;',
                'middle' => '&#xFEA8;',
                'end' => '&#xFEA6;',
                'isolated' => '&#xFEA5;'
            ),
            'د' => array(
                'middle' => '&#xFEAA;',
                'isolated' => '&#xFEA9;'
            ),
            'ذ' => array(
                'middle' => '&#xFEAC;',
                'isolated' => '&#xFEAB;'
            ),
            'ر' => array(
                'middle' => '&#xFEAE;',
                'isolated' => '&#xFEAD;'
            ),
            'ز' => array(
                'middle' => '&#xFEB0;',
                'isolated' => '&#xFEAF;'
            ),
            'س' => array(
                'beginning' => '&#xFEB3;',
                'middle' => '&#xFEB4;',
                'end' => '&#xFEB2;',
                'isolated' => '&#xFEB1;'
            ),
            'ش' => array(
                'beginning' => '&#xFEB7;',
                'middle' => '&#xFEB8;',
                'end' => '&#xFEB6;',
                'isolated' => '&#xFEB5;'
            ),
            'ص' => array(
                'beginning' => '&#xFEBB;',
                'middle' => '&#xFEBC;',
                'end' => '&#xFEBA;',
                'isolated' => '&#xFEB9;'
            ),
            'ض' => array(
                'beginning' => '&#xFEBF;',
                'middle' => '&#xFEC0;',
                'end' => '&#xFEBE;',
                'isolated' => '&#xFEBD;'
            ),
            'ط' => array(
                'beginning' => '&#xFEC3;',
                'middle' => '&#xFEC4;',
                'end' => '&#xFEC2;',
                'isolated' => '&#xFEC1;'
            ),
            'ظ' => array(
                'beginning' => '&#xFEC7;',
                'middle' => '&#xFEC8;',
                'end' => '&#xFEC6;',
                'isolated' => '&#xFEC5;'
            ),
            'ع' => array(
                'beginning' => '&#xFECB;',
                'middle' => '&#xFECC;',
                'end' => '&#xFECA;',
                'isolated' => '&#xFEC9;'
            ),
            'غ' => array(
                'beginning' => '&#xFECF;',
                'middle' => '&#xFED0;',
                'end' => '&#xFECE;',
                'isolated' => '&#xFECD;'
            ),
            'ف' => array(
                'beginning' => '&#xFED3;',
                'middle' => '&#xFED4;',
                'end' => '&#xFED2;',
                'isolated' => '&#xFED1;'
            ),
            'ق' => array(
                'beginning' => '&#xFED7;',
                'middle' => '&#xFED8;',
                'end' => '&#xFED6;',
                'isolated' => '&#xFED5;'
            ),
            'ك' => array(
                'beginning' => '&#xFEDB;',
                'middle' => '&#xFEDC;',
                'end' => '&#xFEDA;',
                'isolated' => '&#xFED9;'
            ),
            'ل' => array(
                'beginning' => '&#xFEDF;',
                'middle' => '&#xFEE0;',
                'end' => '&#xFEDE;',
                'isolated' => '&#xFEDD;'
            ),
            'م' => array(
                'beginning' => '&#xFEE3;',
                'middle' => '&#xFEE4;',
                'end' => '&#xFEE2;',
                'isolated' => '&#xFEE1;'
            ),
            'ن' => array(
                'beginning' => '&#xFEE7;',
                'middle' => '&#xFEE8;',
                'end' => '&#xFEE6;',
                'isolated' => '&#xFEE5;'
            ),
            'ه' => array(
                'beginning' => '&#xFEEB;',
                'middle' => '&#xFEEC;',
                'end' => '&#xFEEA;',
                'isolated' => '&#xFEE9;'
            ),
            'و' => array(
                'middle' => '&#xFEEE;',
                'isolated' => '&#xFEED;'
            ),
            'ي' => array(
                'beginning' => '&#xFEF3;',
                'middle' => '&#xFEF4;',
                'end' => '&#xFEF2;',
                'isolated' => '&#xFEF1;'
            ),
            'ئ' => array(
                'beginning' => '&#xFE8B;',
                'middle' => '&#xFE8C;',
                'end' => '&#xFE8A;',
                'isolated' => '&#xFE89;'
            ),
            'ة' => array(
                'middle' => '&#xFE94;',
                'isolated' => '&#xFE93;'
            )
        );

        if (in_array($word[0] . $word[1], $isolated_chars)) {
            $new_word[] = $all_chars[$word[0] . $word[1]]['isolated'];
            $char_type[] = 'not_normal';
        } else {
            $new_word[] = $all_chars[$word[0] . $word[1]]['beginning'];
            $char_type[] = 'normal';
        }

        if (strlen($word) > 4) {
            if ($char_type[0] == 'not_normal') {
                if (in_array($word[2] . $word[3], $isolated_chars)) {
                    $new_word[] = $all_chars[$word[2] . $word[3]]['isolated'];
                    $char_type[] = 'not_normal';
                } else {

                    $new_word[] = $all_chars[$word[2] . $word[3]]['beginning'];
                    $char_type[] = 'normal';
                }
            } else {
                $new_word[] = $all_chars[$word[2] . $word[3]]['middle'];
                $chars_statue[] = 'middle';

                if (in_array($word[2] . $word[3], $isolated_chars)) {
                    $char_type[] = 'not_normal';
                } else {
                    $char_type[] = 'normal';
                }
            }
            $x = 4;
        } else {
            $x = 2;
        }

        for ($x = 4; $x < (strlen($word) - 4); $x++) {
            if ($char_type[count($char_type) - 1] == 'not_normal' AND $x % 2 == 0) {
                if (in_array($word[$x] . $word[$x + 1], $isolated_chars)) {

                    $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['isolated'];
                    $char_type[] = 'not_normal';
                } else {

                    $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['beginning'];
                    $char_type[] = 'normal';
                }
            } elseif ($char_type[count($char_type) - 1] == 'normal' AND $x % 2 == 0) {

                if (in_array($word[$x] . $word[$x + 1], $isolated_chars)) {

                    $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['middle'];
                    $char_type[] = 'not_normal';
                } else {

                    $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['middle'];
                    $char_type[] = 'normal';
                }
            }

        }
        if (strlen($word) > 6) {
            if ($char_type[count($char_type) - 1] == 'not_normal') {
                if (in_array($word[$x] . $word[$x + 1], $isolated_chars)) {

                    $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['isolated'];
                    $char_type[] = 'not_normal';
                } else {

                    if ($word[strlen($word) - 2] . $word[strlen($word) - 1] == 'ء') {
                        $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['isolated'];
                        $char_type[] = 'normal';
                    } else {
                        $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['beginning'];
                        $char_type[] = 'normal';
                    }

                }

                $x += 2;
            } elseif ($char_type[count($char_type) - 1] == 'normal') {

                if (in_array($word[$x] . $word[$x + 1], $isolated_chars)) {

                    $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['middle'];
                    $char_type[] = 'not_normal';
                } else {

                    $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['middle'];
                    $char_type[] = 'normal';
                }

                $x += 2;
            }


        }

        if ($char_type[count($char_type) - 1] == 'not_normal') {

            if (in_array($word[$x] . $word[$x + 1], $isolated_chars)) {

                $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['isolated'];

            } else {
                $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['isolated'];

            }

        } else {
            if (in_array($word[$x] . $word[$x + 1], $isolated_chars)) {

                $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['middle'];

            } else {

                $new_word[] = $all_chars[$word[$x] . $word[$x + 1]]['end'];

            }
        }

        return implode('', array_reverse($new_word));
    }
}
