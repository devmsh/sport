<?php

namespace App\Http\Controllers\API;

use App\Match;
use App\MatchTeam;
use App\Season;
use App\StagePrediction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeasonStageController extends Controller
{
    public function __construct()
    {
        if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {
            $this->middleware('auth:api');
        }
    }

    public function index(Season $season)
    {
        $stages = $season->stages()->with('match')->get();
        if(\Auth::check()){
            foreach ($stages as $stage) {
                $pred = StagePrediction::where('user_id', \Auth::id())
                    ->where('stage_id', $stage->id)
                    ->first();
                $stage->points = 0;
                if($pred){
                    $stage->points = $pred->weight;
                }
            }
        }
        return $stages;
    }
}
