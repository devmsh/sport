<?php

namespace App\Http\Controllers\API;

use App\Favorite;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\SaveFavoriteRequest;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{

    public function index()
    {
        return Auth::user()->favorites()->get();
    }

    public function store(SaveFavoriteRequest $request)
    {
        return [
            'status' => true,
            'result' => [
                'action' => Favorite::toggle(Auth::user(), $request->type, $request->id),
            ]
        ];
    }
}
