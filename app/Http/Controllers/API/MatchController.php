<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\AddMatchResultRequest;
use App\Http\Requests\API\MatchesListRequest;
use App\Http\Requests\API\PredictMatchResultRequest;
use App\Http\Requests\API\PredictStageRequest;
use App\Match;
use App\MatchTeamPrediction;
use App\Repositories\MatchesRepository;
use App\Season;
use App\Stage;
use App\StagePrediction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MatchController extends Controller
{
    public function __construct()
    {
        if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {
            $this->middleware('auth:api');
        }
    }

    public function index(MatchesListRequest $request)
    {
        $date = $request->get('started_at', Carbon::today()->format("d-m-Y"));
        $date = $this->indicToArabic($date);

        return MatchesRepository::getMatches($date, $request);
    }

    public function best(MatchesListRequest $request)
    {
        $date = $request->get('started_at', Carbon::today()->format("d-m-Y"));
        $date = $this->indicToArabic($date);

        return MatchesRepository::getBestMatches($date, $request);
    }

    public function show(Match $match)
    {
        $match->append('stats');
        $match->load('myTeamPrediction.firstTeam','myTeamPrediction.secondTeam');
        return $match;
    }

    function indicToArabic($str)
    {
        $arabic = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $indic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
        return str_replace($indic, $arabic, $str);
    }
}
