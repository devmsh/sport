<?php

namespace App\Http\Controllers\API;

use App\Suggestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuggestionController extends Controller
{
    public function store($type, Request $request)
    {
        $suggestion = Suggestion::firstOrCreate([
            'type' => $type,
            'name' => $request->name,
            'user_id' => \Auth::id()
        ]);

        $suggestion->save();

        return [
            'status' => true,
            'message' => "Thanks for suggestions, we will review it very soon"
        ];
    }
}
