<?php

namespace App\Http\Requests\API;

use App\Rules\FavoriteableExist;

class SaveFavoriteRequest extends SuperRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'integer', new FavoriteableExist($this->type)],
            'type' => 'required|in:league,team,match'
        ];
    }
}