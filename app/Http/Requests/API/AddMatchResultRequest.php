<?php

namespace App\Http\Requests\API;

use App\Rules\IsMatchFinished;

class AddMatchResultRequest extends SuperRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'match' => [new IsMatchFinished(105)]
        ];
    }

    protected function validationData()
    {
        $this->merge($this->route()->parameters());
        return $this->all();
    }
}