<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prediction extends Model {

    protected $guarded = [];

    public function predictable() {
        return $this->morphTo();
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
