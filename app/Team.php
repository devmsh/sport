<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Spatie\Translatable\HasTranslations;

class Team extends Model
{
    use CrudTrait, HasTranslations;

    public $translatable = ['name'];

    protected $guarded = [];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function seasons()
    {
        return $this->belongsToMany(Season::class)->withPivot('group');
    }

    public function homeMatches()
    {
        return $this->hasMany(Match::class, 'first_team_id');
    }

    public function awayMatches()
    {
        return $this->hasMany(Match::class, 'second_team_id');
    }

    public function leagues()
    {
        return $this->belongsToMany(League::class);
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'tname' => $this->tname,
            'logo' => $this->logo,
            'followers_count' => $this->followers_count,
        ];
    }

    public function getLogoAttribute($value)
    {
        return $value ? url('storage/' . $value) : null;
    }

    public function setLogoAttribute($value)
    {
        if ($value == null) {
            $this->attributes["logo"] = null;
        }

        if (starts_with($value, 'data:image')) {
            $filename = $this->getRoundedLogo($value);
            $this->attributes["logo"] = $filename;
        } else {
            $this->attributes["logo"] = $value;
        }
    }

    public function getRoundedLogo($value)
    {
        $img = Image::make($value)->resize(200, 200);

        $width = $img->getWidth();
        $height = $img->getHeight();

        $mask = Image::canvas($width, $height);

        // draw a white circle
        $mask->circle($width, $width / 2, $height / 2, function ($draw) {
            $draw->background('#fff');
        });

        $img->mask($mask, true);
        $img->resize(100, 100);

        $file_name = "uploads/teams/{$this->id}.png";
        $img->save(public_path($file_name));

        return $file_name;
    }
}
