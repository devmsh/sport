<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Leaderboard extends Model
{
    protected $with = ["user"];

    public static function score($user)
    {
        $points = Leaderboard::where('user_id', $user->id)->where('status', '!=', 1)->sum('points');

        return Leaderboard::where('status', '!=', 1)
            ->where('points', '!=', 1000)
            ->groupBy('user_id')
            ->havingRaw('sum(points) > ?', [$points])
            ->count('*');
    }

    public static function top($limit = 10, $for = null, $excludeNew = true)
    {
        $leaderboard = Leaderboard::select(DB::raw('user_id, sum(points) as points1'))
            ->limit($limit)
            ->groupBy('user_id')
            ->orderBy('points1', 'desc');

        if ($excludeNew) {
            $leaderboard = $leaderboard->having('points1', '!=', 1000);
        }

        $initial = 1000;

        if ($for) {
            $initial = 0;
            switch (get_class($for)) {
                case Season::class:
                    $leaderboard->where('season_id', $for->id);
                    break;
                case League::class:
                    $leaderboard->where('league_id', $for->id);
                    break;
                case Stage::class:
                    $leaderboard->where('stage_id', $for->id);
                    break;
                case Team::class:
                    $leaderboard->where('first_team_id', $for->id)
                        ->orWhere('second_team_id', $for->id);
                    break;
            }
        }

        $leaders = $leaderboard->get();

        if ($leaders->count() == 0 && $excludeNew) {
            return Leaderboard::top($limit, $for, false);
        }

        $users = collect([]);
        foreach ($leaders as $key => $leader) {
            $user = $leader->user->toArray();
            $user['rank'] = $key + 1;
            $user['points'] = $initial + $leader->points1;
            $users [] = $user;
        }
        return $users;
    }

    public static function points($user)
    {
        return Leaderboard::where('status', '!=', 1)
                ->where('user_id', $user->id)
                ->sum('points') + 1000;
    }

    public static function ranking(User $user)
    {
        return Leaderboard::select(DB::raw('stage_id, sum(points) as points1'))
            ->where('user_id', $user->id)
            ->groupBy('stage_id')
            ->orderBy('points1', 'desc')
            ->get();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
