<?php

namespace App\Listeners;

use App\Events\MatchIdsSet;
use App\Events\StageFinished;
use App\Match;
use App\MatchTeam;
use Illuminate\Contracts\Queue\ShouldQueue;

class MatchTeamEvaluationListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MatchIdsSet $event
     * @return void
     */
    public function handle(MatchIdsSet $event)
    {
        $event->matchTeam->evaluateTeams();
        if (!Match::getUnSetTeamsNumber($event->matchTeam->stage_id))
            event(new StageFinished($event->matchTeam->stage));
    }
}
