<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i>
        <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i>
        <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>

<li>
    <a href="{{ backpack_url('association') }}">
        <i class="fa fa-dashboard"></i>
        <span>{{ trans('association') }}</span>
    </a>
</li>
<li>
    <a href="{{ backpack_url('country') }}">
        <i class="fa fa-dashboard"></i>
        <span>{{ trans('country') }}</span>
    </a>
</li>
{{--<li>--}}
    {{--<a href="{{ backpack_url('favorite') }}">--}}
        {{--<i class="fa fa-dashboard"></i>--}}
        {{--<span>{{ trans('favorite') }}</span>--}}
    {{--</a>--}}
{{--</li>--}}
<li>
    <a href="{{ backpack_url('league') }}">
        <i class="fa fa-dashboard"></i>
        <span>{{ trans('league') }}</span>
    </a>
</li>
<li>
    <a href="{{ backpack_url('team') }}">
        <i class="fa fa-dashboard"></i>
        <span>{{ trans('team') }}</span>
    </a>
</li>
<li>
    <a href="{{ backpack_url('match') }}">
        <i class="fa fa-dashboard"></i>
        <span>{{ trans('match') }}</span>
    </a>
</li>
<li>
    <a href="{{ backpack_url('matchresultprediction') }}">
        <i class="fa fa-dashboard"></i>
        <span>{{ trans('matchResultPrediction') }}</span>
    </a>
</li>
<li>
    <a href="{{ backpack_url('matchteam') }}">
        <i class="fa fa-dashboard"></i>
        <span>{{ trans('matchTeam') }}</span>
    </a>
</li>
<li>
    <a href="{{ backpack_url('point') }}">
        <i class="fa fa-dashboard"></i>
        <span>{{ trans('point') }}</span>
    </a>
</li>
<li>
    <a href="{{ backpack_url('prediction') }}">
        <i class="fa fa-dashboard"></i>
        <span>{{ trans('prediction') }}</span>
    </a>
</li>

<li>
    <a href="{{ backpack_url('user') }}">
        <i class="fa fa-dashboard"></i>
        <span>{{ trans('user') }}</span>
    </a>
</li>
<li>
    <a href="{{ backpack_url('usermatchresult') }}">
        <i class="fa fa-dashboard"></i>
        <span>{{ trans('userMatchResult') }}</span>
    </a>
</li>
