<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchTeamPredictionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_team_predictions', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->unsignedInteger('stage_id');
            $table->unsignedInteger('stage_prediction_id');
            $table->unsignedInteger('match_id');

            $table->unsignedInteger('first_team_id')->nullable();
            $table->unsignedInteger('second_team_id')->nullable();

            $table->integer('status');
            $table->float('weight')->default(0);
            $table->float('points')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_team_predictions');
    }
}
