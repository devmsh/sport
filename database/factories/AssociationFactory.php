<?php

use Faker\Generator as Faker;

$factory->define(App\Association::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'logo' => $faker->imageUrl(300,300),
    ];
});
