<?php

use Faker\Generator as Faker;

$factory->define(App\Team::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'shortcut' => $faker->randomAscii,
        'logo' => $faker->imageUrl(),
        'followers_count' => rand(10,10000),
    ];
});
