<?php

use Faker\Generator as Faker;

$factory->define(App\Country::class, function (Faker $faker) {
    return [
        'name' => $faker->country,
        'logo' => $faker->imageUrl(300,300),
        'shortcut' => 'PS'
    ];
});
