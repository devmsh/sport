<?php

use Faker\Generator as Faker;

$factory->define(App\Season::class, function (Faker $faker) {
    return [
        'name' => $faker->country,
        'logo' => $faker->imageUrl(),
        'league_id' => function(){
            return factory(\App\League::class)->create();
        }
    ];
});
