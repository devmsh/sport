<?php

use Faker\Generator as Faker;

$factory->define(App\Stage::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(["32 stage","16 stage", "1/4 final", "1/2 final", "final"]),
        'type' => $faker->randomElement([\App\Stage::STAGE_8,\App\Stage::STAGE_16]),
        'weight' => $faker->numberBetween(100,500),
        'evaluated' => false,
        'logo' => $faker->imageUrl(),
        'season_id' => function(){
            return factory(\App\Season::class)->create()->id;
        },
        'can_draw' => 1
    ];
});
