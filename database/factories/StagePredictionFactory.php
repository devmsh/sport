<?php

use Faker\Generator as Faker;

$factory->define(App\StagePrediction::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return factory(\App\User::class)->create()->id;
        },
        'stage_id' => function(){
            return factory(\App\Stage::class)->create()->id;
        },
        'finished_matches' => 0,
        'json' => '{}'
    ];
});
