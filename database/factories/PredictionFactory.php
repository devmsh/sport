<?php

use Faker\Generator as Faker;

$factory->define(App\Prediction::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return factory(\App\User::class)->create();
        },
        'predictable_id' => function(){
            return factory(\App\MatchResultPrediction::class)->create();
        },
        'predictable_type' => \App\MatchResultPrediction::class,
    ];
});
