<?php

use Faker\Generator as Faker;

$factory->define(App\UserStory::class, function (Faker $faker) {
    return [
        'image' => $faker->imageUrl(300,300),
        'title' => $faker->sentence(3),
        'description' => $faker->sentence(20),
    ];
});
